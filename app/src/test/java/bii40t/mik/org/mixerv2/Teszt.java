package bii40t.mik.org.mixerv2;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import bii40t.mik.org.mixerv2.addNewCocktailFragment.AddNewCocktailFragmentInterface;
import bii40t.mik.org.mixerv2.addNewCocktailFragment.AddNewCocktailPresenter;
import bii40t.mik.org.mixerv2.model.favorite.FavouriteCocktail;
import bii40t.mik.org.mixerv2.model.favorite.FavouriteCocktailService;
import bii40t.mik.org.mixerv2.login.LogInPresenter;
import bii40t.mik.org.mixerv2.login.LogInViewInterface;
import bii40t.mik.org.mixerv2.login.LogInActivity;
import bii40t.mik.org.mixerv2.model.ingredient.Ingredient;
import bii40t.mik.org.mixerv2.model.recipe.Recipe;
import bii40t.mik.org.mixerv2.model.userCocktail.UserCocktail;
import bii40t.mik.org.mixerv2.model.userCocktail.UserCocktailService;
import bii40t.mik.org.mixerv2.signup.SignUpActivity;
import bii40t.mik.org.mixerv2.signup.SignUpPresenter;
import bii40t.mik.org.mixerv2.signup.SignUpViewInterface;
import bii40t.mik.org.mixerv2.model.user.User;
import bii40t.mik.org.mixerv2.model.user.UserService;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

public class Teszt {

    private LogInPresenter logInPresent;
    private SignUpPresenter signUpPresenter;
    private AddNewCocktailPresenter addNewCocktailPresenter;
    private Drawable drawable;
    private Context context;
    private MockWebServer mockWebServer;
    private Retrofit retrofit;

    @Mock
    private LogInActivity signInActivity;

    @Mock
    private SignUpActivity signUpActivity;

    @Mock
    private User user;

    @Mock
    private LogInViewInterface logInViewInterface;

    @Mock
    private AddNewCocktailFragmentInterface addNewCocktailFragmentInterface;

    @Mock
    private SignUpViewInterface signUpView;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        user = Mockito.mock(User.class);
        Gson gson = new GsonBuilder().setLenient().create();
        mockWebServer = new MockWebServer();
        retrofit = new Retrofit.Builder()
                .baseUrl(mockWebServer.url("/").toString())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        context = Mockito.mock(Context.class);
        LinearLayout linearLayout = Mockito.mock(LinearLayout.class);
        logInViewInterface = Mockito.mock(LogInViewInterface.class);
        logInPresent = new LogInPresenter(logInViewInterface);
        signUpPresenter = new SignUpPresenter(signUpView);
        addNewCocktailPresenter = new AddNewCocktailPresenter(addNewCocktailFragmentInterface, context,
                linearLayout, user);
        drawable = Mockito.mock(Drawable.class);
    }

    @After
    public void close() throws Exception {
        mockWebServer.shutdown();
    }

    @Test
    public void shouldShowErrorMessageWhenUserNameIsEmpty() {
        char[] password = {'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a'};
        logInPresent.checkLoginDetail("", password);
        Mockito.verify(logInViewInterface).showErrorMessage(R.string.emptyUserName);
    }

    @Test
    public void shouldShowErrorMessageWhenPasswordIsEmpty() {
        char[] password = {};
        logInPresent.checkLoginDetail("Teszt", password);
        Mockito.verify(logInViewInterface).showErrorMessage(R.string.emptyPassWd);
    }

    @Test
    public void shouldShowErrorMessageWhenPasswordIsShort() {
        char[] password = {'a'};
        logInPresent.checkLoginDetail("Teszt", password);
        Mockito.verify(logInViewInterface).showErrorMessage(R.string.shortPasswd);
    }

    @Test
    public void loginFailwithMockWebServer() throws Exception {
        mockWebServer.enqueue(new MockResponse().setBody("COCKTAIL"));
        MockResponse mockResponse = new MockResponse()
                .setHeader("Content-Type", "application/json; charset=utf-8")
                .setHeader("Cache-Control", "no-cache")
                .setBody("Incorrect");
        mockResponse.throttleBody(1024, 1, TimeUnit.SECONDS);

        UserService service = retrofit.create(UserService.class);
        user = Mockito.mock(User.class);
        Call<String> call = service.login(user);
        assertTrue(call.execute() != null);

        String response = String.valueOf(mockResponse.getBody());
        assertEquals("[text=Incorrect]", response);
    }

    @Test
    public void loginSuccessful() {
        char[] password = {'a', 'a', 'a', 'a', 'a', 'a', 'a'};
        signInActivity = Mockito.mock(LogInActivity.class);
        logInPresent.checkLoginDetail("Teszt", password);
        signInActivity.startActivity(user);
        Mockito.verify(signInActivity).startActivity(user);
    }

    @Test
    public void loginSuccessfulwithMockWebServer() throws Exception {
        mockWebServer.enqueue(new MockResponse().setBody("COCKTAIL"));
        MockResponse mockResponse = new MockResponse()
                .setHeader("Content-Type", "application/json; charset=utf-8")
                .setHeader("Cache-Control", "no-cache")
                .setBody("Ok");
        mockResponse.throttleBody(1024, 1, TimeUnit.SECONDS);

        UserService service = retrofit.create(UserService.class);
        Call<String> call = service.login(user);
        assertTrue(call.execute() != null);

        String response = String.valueOf(mockResponse.getBody());
        assertEquals("[text=Ok]", response);
    }

    @Test
    public void shouldShowErrorMessageWhenUserNameIsEmptyInSignUp() {
        String password = "12345678";
        String password2 = "12345678";
        signUpPresenter.checkSignUpDetails("", password, password2);
        Mockito.verify(signUpView).showErrorMessage(R.string.emptyUserName);
    }

    @Test
    public void shouldShowErrorMessageWhenPasswordIsEmptyInSignUp() {
        String password = "";
        String password2 = "";
        signUpPresenter.checkSignUpDetails("Teszt", password, password2);
        Mockito.verify(signUpView).showErrorMessage(R.string.emptyPassWd);
    }

    @Test
    public void shouldShowErrorMessageWhenPasswordIsShortInSignUp() {
        String password = "123";
        String password2 = "";
        signUpPresenter.checkSignUpDetails("Teszt", password, password2);
        Mockito.verify(signUpView).showErrorMessage(R.string.shortPasswd);
    }

    @Test
    public void shouldShowErrorMessageWhenPasswordsAreDifferent() {
        String password = "12345678";
        String password2 = "";
        signUpPresenter.checkSignUpDetails("Teszt", password, password2);
        Mockito.verify(signUpView).showErrorMessage(R.string.diffPassWd);
    }

    @Test
    public void signUpSuccessful() {
        String password = "1234567";
        String password2 = "1234567";
        signUpPresenter.checkSignUpDetails("Teszt", password, password2);
        signUpActivity = Mockito.mock(SignUpActivity.class);
        signUpActivity.startActivity(user);
        Mockito.verify(signUpActivity).startActivity(user);
    }

    @Test
    public void signUpWhenUserAlreadyExist() {
        String password = "1234567";
        String password2 = "1234567";
        signUpPresenter.checkSignUpDetails("Teszt", password, password2);
        signUpActivity = Mockito.mock(SignUpActivity.class);
        signUpActivity.showErrorMessage(R.string.alreadyExist);
        Mockito.verify(signUpActivity).showErrorMessage(R.string.alreadyExist);
    }

    @Test
    public void signUpSuccessfulWithMockWebServer() throws Exception {
        mockWebServer.enqueue(new MockResponse().setBody("COCKTAIL"));
        MockResponse mockResponse = new MockResponse()
                .setHeader("Content-Type", "application/json; charset=utf-8")
                .setHeader("Cache-Control", "no-cache")
                .setBody("ok");
        mockResponse.throttleBody(1024, 1, TimeUnit.SECONDS);

        UserService service = retrofit.create(UserService.class);
        Call<String> call = service.createUser(user);
        assertTrue(call.execute() != null);

        String response = String.valueOf(mockResponse.getBody());
        assertEquals("[text=ok]", response);
    }

    @Test
    public void signUpWhenUserAlreadyExistWithMockWebServer() throws Exception {
        mockWebServer.enqueue(new MockResponse().setBody("COCKTAIL"));
        MockResponse mockResponse = new MockResponse()
                .setHeader("Content-Type", "application/json; charset=utf-8")
                .setHeader("Cache-Control", "no-cache")
                .setBody("alreadyExists");
        mockResponse.throttleBody(1024, 1, TimeUnit.SECONDS);

        UserService service = retrofit.create(UserService.class);
        Call<String> call = service.createUser(user);
        assertTrue(call.execute() != null);

        String response = String.valueOf(mockResponse.getBody());
        assertEquals("[text=alreadyExists]", response);
    }

    @Test
    public void getCocktailFromServer() throws Exception {
        char[] password = {'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a'};
        User user = new User("Teszt", password);
        List<UserCocktail> cocktails = new ArrayList<>();
        List<Recipe> recipes = new ArrayList<>();
        recipes.add(new Recipe(1, new Ingredient(1, "ingredient 1")));
        cocktails.add(new UserCocktail("Cocktal name", "category",
                "description", "imageurl", recipes,false, user));
        Gson gson = new Gson();
        String json = gson.toJson(cocktails);
        mockWebServer.enqueue(new MockResponse().setBody(json));
        MockResponse mockResponse = new MockResponse()
                .setHeader("Content-Type", "application/json; charset=utf-8")
                .setHeader("Cache-Control", "no-cache");
        mockResponse.throttleBody(1024, 1, TimeUnit.SECONDS);

        UserCocktailService service = retrofit.create(UserCocktailService.class);
        Call<List<UserCocktail>> call = service.getCocktails();
        assertTrue(call.execute() != null);
    }

    @Test
    public void getUserCocktailFromServer() throws Exception {
        char[] password = {'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a'};
        User user = new User("Teszt", password);
        List<UserCocktail> cocktails = new ArrayList<>();
        List<Recipe> recipes = new ArrayList<>();
        recipes.add(new Recipe(1, new Ingredient(1, "ingredient 1")));
        cocktails.add(new UserCocktail("Cocktal name", "category",
                "description", "imageurl", recipes,false, user));
        Gson gson = new Gson();
        String json = gson.toJson(cocktails);
        mockWebServer.enqueue(new MockResponse().setBody(json));
        MockResponse mockResponse = new MockResponse()
                .setHeader("Content-Type", "application/json; charset=utf-8")
                .setHeader("Cache-Control", "no-cache");
        mockResponse.throttleBody(1024, 1, TimeUnit.SECONDS);

        UserCocktailService service = retrofit.create(UserCocktailService.class);
        Call<List<UserCocktail>> call = service.getUserCocktails(user.getUsername());
        assertTrue(call.execute() != null);
    }

    @Test
    public void getFavouriteCocktailFromServer() throws Exception {
        char[] password = {'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a'};
        User user = new User("Teszt", password);

        List<FavouriteCocktail> favouriteCocktails = new ArrayList<>();
        favouriteCocktails.add(new FavouriteCocktail(1, "Teszt2", user));
        Gson gson = new Gson();
        String json = gson.toJson(favouriteCocktails);

        mockWebServer.enqueue(new MockResponse().setBody(json));
        MockResponse mockResponse = new MockResponse()
                .setHeader("Content-Type", "application/json; charset=utf-8")
                .setHeader("Cache-Control", "no-cache")
                .setBody(json);
        mockResponse.throttleBody(1024, 1, TimeUnit.SECONDS);

        FavouriteCocktailService service = retrofit.create(FavouriteCocktailService.class);
        Call<List<FavouriteCocktail>> call = service.getFavourites(user.getUsername());
        assertTrue(call.execute() != null);
    }

    @Test
    public void deleteFavouriteCocktailFromServer() throws Exception {
        mockWebServer.enqueue(new MockResponse().setBody("ok"));
        MockResponse mockResponse = new MockResponse()
                .setHeader("Content-Type", "application/json; charset=utf-8")
                .setHeader("Cache-Control", "no-cache");
        mockResponse.throttleBody(1024, 1, TimeUnit.SECONDS);

        FavouriteCocktailService service = retrofit.create(FavouriteCocktailService.class);
        Call<Void> call = service.deleteFavourites("identifier");
        assertTrue(call.execute() != null);
    }

    @Test
    public void saveFavouriteCocktailFromServer() throws Exception {
        char[] password = {'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a'};
        User user = new User("Teszt", password);
        FavouriteCocktail favouriteCocktail = new FavouriteCocktail(1, "Teszt2", user);

        mockWebServer.enqueue(new MockResponse().setBody("ok"));
        MockResponse mockResponse = new MockResponse()
                .setHeader("Content-Type", "application/json; charset=utf-8")
                .setHeader("Cache-Control", "no-cache");
        mockResponse.throttleBody(1024, 1, TimeUnit.SECONDS);

        FavouriteCocktailService service = retrofit.create(FavouriteCocktailService.class);
        Call<Void> call = service.saveFavourites(favouriteCocktail);
        assertTrue(call.execute() != null);
    }

    @Test
    public void shouldShowErrorMessageWhenNewCocktailNameIsEmpty() {
        addNewCocktailPresenter.checkNewCocktailDetails("", "description",
                "unit1", "unit2", drawable);
        Mockito.verify(addNewCocktailFragmentInterface).showErrorMessage(R.string.noCocktailName);
    }

    @Test
    public void shouldShowErrorMessageWhenNewCocktailDescriptionIsEmpty() {
        addNewCocktailPresenter.checkNewCocktailDetails("Teszt", "",
                "unit1", "unit2",  drawable);
        Mockito.verify(addNewCocktailFragmentInterface).showErrorMessage(R.string.noDescription);
    }

    @Test
    public void shouldShowErrorMessageWhenUnit1IsEmpty() {
        addNewCocktailPresenter.checkNewCocktailDetails("Teszt", "Description",
                "", "unit2",  drawable);
        Mockito.verify(addNewCocktailFragmentInterface).showErrorMessage(R.string.noUnit);
    }

    @Test
    public void shouldShowErrorMessageWhenUnit2IsEmpty() {
        addNewCocktailPresenter.checkNewCocktailDetails("Teszt", "Description",
                "2", "", drawable);
        Mockito.verify(addNewCocktailFragmentInterface).showErrorMessage(R.string.noUnit);
    }

    @Test
    public void shouldShowErrorMessageWhenDrawableIsNull() {
        drawable = null;
        addNewCocktailPresenter.checkNewCocktailDetails("Teszt", "Description",
                "2", "", drawable);
        Mockito.verify(addNewCocktailFragmentInterface).showErrorMessage(R.string.noImage);
    }

    @Test
    public void uploadUserCocktail() throws IOException {
        char[] password = {'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a'};
        User user = new User("Teszt", password);
        List<Recipe> recipes = new ArrayList<>();
        recipes.add(new Recipe(1, new Ingredient(1, "ingredient 1")));
        UserCocktail userCocktail = new UserCocktail("Cocktal name", "category",
                "description", "imageurl", recipes,false, user);

        mockWebServer.enqueue(new MockResponse().setBody("Ok"));
        MockResponse mockResponse = new MockResponse()
                .setHeader("Content-Type", "application/json; charset=utf-8")
                .setHeader("Cache-Control", "no-cache");
        mockResponse.throttleBody(1024, 1, TimeUnit.SECONDS);

        UserCocktailService service = retrofit.create(UserCocktailService.class);
        File f = Mockito.mock(File.class);
        RequestBody reqFile = Mockito.mock(RequestBody.class);
        MultipartBody.Part part  = MultipartBody.Part.createFormData("file", f.getName(), reqFile);
        Call<String> call = service.upload(userCocktail,part);
        assertTrue(call.execute() != null);
    }

    @Test
    public void editUserCocktail() throws IOException {
        int id = 1;
        char[] password = {'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a'};
        User user = new User("Teszt", password);
        List<Recipe> recipes = new ArrayList<>();
        recipes.add(new Recipe(1, new Ingredient(1, "ingredient 1")));
        UserCocktail userCocktail = new UserCocktail("Cocktal name", "category",
                "description", "imageurl", recipes,false, user);

        mockWebServer.enqueue(new MockResponse().setBody("Ok"));
        MockResponse mockResponse = new MockResponse()
                .setHeader("Content-Type", "application/json; charset=utf-8")
                .setHeader("Cache-Control", "no-cache");
        mockResponse.throttleBody(1024, 1, TimeUnit.SECONDS);

        UserCocktailService service = retrofit.create(UserCocktailService.class);
        File f = Mockito.mock(File.class);
        RequestBody reqFile = Mockito.mock(RequestBody.class);
        MultipartBody.Part part  = MultipartBody.Part.createFormData("file", f.getName(), reqFile);
        Call<String> call = service.editUserCocktail(id,userCocktail,part);
        assertTrue(call.execute() != null);
    }

    @Test
    public void deleteUserCocktail() throws IOException {
        int id = 1;
        mockWebServer.enqueue(new MockResponse().setBody("Ok"));
        MockResponse mockResponse = new MockResponse()
                .setHeader("Content-Type", "application/json; charset=utf-8")
                .setHeader("Cache-Control", "no-cache");
        mockResponse.throttleBody(1024, 1, TimeUnit.SECONDS);

        UserCocktailService service = retrofit.create(UserCocktailService.class);
        Call<Void> call = service.deleteUserCocktail(id);
        assertTrue(call.execute() != null);
    }

    @Test
    public void saveUserCocktailToLocalDatabase(){
        List<Recipe> recipes = new ArrayList<>();
        recipes.add(new Recipe(1, new Ingredient(1, "ingredient 1")));
        UserCocktail userCocktail = new UserCocktail("Cocktal name", "category",
                "escription", "imageurl", recipes,false ,user);
        addNewCocktailPresenter.saveCocktailToLocalDatabase(userCocktail);
    }

    @Test
    public void shareUserCocktail() throws IOException {
        int id = 1;
        mockWebServer.enqueue(new MockResponse().setBody("Ok"));
        MockResponse mockResponse = new MockResponse()
                .setHeader("Content-Type", "application/json; charset=utf-8")
                .setHeader("Cache-Control", "no-cache");
        mockResponse.throttleBody(1024, 1, TimeUnit.SECONDS);

        UserCocktailService service = retrofit.create(UserCocktailService.class);
        Call<String> call = service.shareCocktail(id);
        assertTrue(call.execute() != null);
    }
}