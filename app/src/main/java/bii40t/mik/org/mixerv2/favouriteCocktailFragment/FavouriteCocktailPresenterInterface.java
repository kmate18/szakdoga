package bii40t.mik.org.mixerv2.favouriteCocktailFragment;

import java.util.List;

import bii40t.mik.org.mixerv2.model.favorite.FavouriteCocktail;

public interface FavouriteCocktailPresenterInterface {

    void getFavouriteCocktailFromServer();

    void saveFavouriteCocktail(List<FavouriteCocktail> favouriteCocktails);

    void getFavouriteCocktailFromLocalDatabase();
}