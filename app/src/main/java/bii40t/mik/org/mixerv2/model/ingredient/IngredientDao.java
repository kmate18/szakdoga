package bii40t.mik.org.mixerv2.model.ingredient;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface IngredientDao {

    @Query("select * from ingredients")
    public List<Ingredient> find();

    @Query("select * from ingredients where ingredients.ingredient_id =:id")
    public Ingredient findById(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void save(Ingredient... ingredient);
}
