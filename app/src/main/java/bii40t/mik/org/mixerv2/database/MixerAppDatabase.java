package bii40t.mik.org.mixerv2.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import bii40t.mik.org.mixerv2.model.cocktail.Cocktail;
import bii40t.mik.org.mixerv2.model.cocktail.CocktailDao;
import bii40t.mik.org.mixerv2.model.favorite.FavouriteCocktail;
import bii40t.mik.org.mixerv2.model.favorite.FavouriteCocktailDao;
import bii40t.mik.org.mixerv2.model.ingredient.Ingredient;
import bii40t.mik.org.mixerv2.model.ingredient.IngredientDao;
import bii40t.mik.org.mixerv2.model.recipe.Recipe;
import bii40t.mik.org.mixerv2.model.recipe.RecipeDao;
import bii40t.mik.org.mixerv2.model.user.User;
import bii40t.mik.org.mixerv2.model.userCocktail.UserCocktail;
import bii40t.mik.org.mixerv2.model.userCocktail.UserCocktailDao;

@Database(entities = { User.class, Cocktail.class, UserCocktail.class, Recipe.class,
        FavouriteCocktail.class, Ingredient.class}, version = 45)
@TypeConverters({Converters.class})
public abstract class MixerAppDatabase extends RoomDatabase {

    public abstract CocktailDao cocktailDao();
    public abstract RecipeDao recipeDao();
    public abstract FavouriteCocktailDao favouriteCocktailDao();
    public abstract IngredientDao ingredientDao();
    public abstract UserCocktailDao userCocktailDao();
}