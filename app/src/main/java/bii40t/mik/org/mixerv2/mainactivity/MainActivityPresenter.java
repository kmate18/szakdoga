package bii40t.mik.org.mixerv2.mainactivity;

import android.content.Context;
import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

import bii40t.mik.org.mixerv2.ApiClient;
import bii40t.mik.org.mixerv2.model.cocktail.Cocktail;
import bii40t.mik.org.mixerv2.database.DatabaseClient;
import bii40t.mik.org.mixerv2.fragment.HomeFragment;
import bii40t.mik.org.mixerv2.model.recipe.Recipe;
import bii40t.mik.org.mixerv2.model.userCocktail.UserCocktail;
import bii40t.mik.org.mixerv2.model.userCocktail.UserCocktailService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivityPresenter implements MainActivityPresenterInterface {

    private MainActivityViewInterface mainActivityViewInterface;
    private Context context;
    private List<Cocktail> cocktails = new ArrayList<>();
    private UserCocktailService userCocktailService = ApiClient.getClient().create(UserCocktailService.class);

    public MainActivityPresenter(MainActivityViewInterface mainActivityViewInterface, Context context) {
        this.mainActivityViewInterface = mainActivityViewInterface;
        this.context = context;
    }

    @Override
    public void getCocktailsFromServer(){
        mainActivityViewInterface.showProgressDialog();
        Call<List<UserCocktail>> call = userCocktailService.getCocktails();
        call.enqueue(new Callback<List<UserCocktail>>() {
            @Override
            public void onResponse(Call<List<UserCocktail>> call,
                                   Response<List<UserCocktail>> response) {
                List<UserCocktail> userCocktails = response.body();
                for (UserCocktail userCocktail : userCocktails) {
                    if (userCocktail.getUser() == null || userCocktail.isShare()) {
                        cocktails.add(userCocktail);
                    }
                }
                mainActivityViewInterface.addCocktailToPreferenciaManager(cocktails);
                mainActivityViewInterface.loadFragment(new HomeFragment(cocktails));
                mainActivityViewInterface.searchList(cocktails);
                mainActivityViewInterface.hideProgressDialog();
                saveCocktails(cocktails);
            }

            @Override
            public void onFailure(Call<List<UserCocktail>> call, Throwable t) {
                t.printStackTrace();
                mainActivityViewInterface.hideProgressDialog();
            }
        });
    }

    @Override
    public void saveCocktails(final List<Cocktail> cocktails) {
        class SaveCocktail extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... voids) {
                List<Cocktail> localCocktails = DatabaseClient.getInstance(context)
                        .getMixerAppDatabase().cocktailDao().findAll();
                for (int i = 0; i < cocktails.size(); i++) {

                    if (!localCocktails.contains(cocktails.get(i))) {
                        DatabaseClient.getInstance(context).getMixerAppDatabase()
                                .cocktailDao()
                                .save(cocktails.get(i));
                        for (Recipe recipe : cocktails.get(i).getRecipes()) {
                            recipe.setCocktail(cocktails.get(i));
                            DatabaseClient.getInstance(context).getMixerAppDatabase()
                                    .recipeDao().save(recipe);
                        }
                    }
                }
                return null;
            }
        }
        SaveCocktail sc = new SaveCocktail();
        sc.execute();
    }

    @Override
    public void getCocktailsFromLocalDatabase() {
        mainActivityViewInterface.showProgressDialog();
        class LoadCocktail extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... voids) {
                List<Cocktail> localCocktails = DatabaseClient.getInstance(context)
                        .getMixerAppDatabase().cocktailDao().findAll();

                for (Cocktail cocktail : localCocktails) {
                    List<Recipe> recipes = DatabaseClient.getInstance(context)
                            .getMixerAppDatabase().recipeDao()
                            .findAllByCocktailId(cocktail.getId());
                    cocktail.setRecipes(recipes);
                    cocktails.add(cocktail);
                }
                mainActivityViewInterface.addCocktailToPreferenciaManager(cocktails);
                mainActivityViewInterface.loadFragment(new HomeFragment());
                mainActivityViewInterface.searchList(cocktails);
                mainActivityViewInterface.hideProgressDialog();
                return null;
            }
        }
        LoadCocktail lc = new LoadCocktail();
        lc.execute();
    }
}