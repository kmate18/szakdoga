package bii40t.mik.org.mixerv2.model.user;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface UserService {

    @POST("registration")
    Call<String> createUser(@Body User user);

    @POST("login")
    Call<String> login(@Body User user);
}
