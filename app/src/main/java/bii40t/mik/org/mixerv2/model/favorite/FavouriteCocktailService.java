package bii40t.mik.org.mixerv2.model.favorite;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface FavouriteCocktailService {

    @POST("favourite/save")
    Call<Void> saveFavourites(@Body FavouriteCocktail favouriteCocktail);

    @DELETE("favourite/delete/{identifier}")
    Call<Void> deleteFavourites(@Path("identifier") String identifier);

    @GET("favourite/user/{userName}")
    Call<List<FavouriteCocktail>> getFavourites(@Path("userName") String userName);
}
