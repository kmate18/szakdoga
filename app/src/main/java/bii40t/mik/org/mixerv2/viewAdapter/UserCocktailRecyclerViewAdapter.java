package bii40t.mik.org.mixerv2.viewAdapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.MemoryPolicy;

import java.util.List;

import bii40t.mik.org.mixerv2.HttpsUtil;
import bii40t.mik.org.mixerv2.mainactivity.MainActivity;
import bii40t.mik.org.mixerv2.PicassoTrustAll;
import bii40t.mik.org.mixerv2.R;
import bii40t.mik.org.mixerv2.database.DatabaseClient;
import bii40t.mik.org.mixerv2.cocktailDetailFragment.CocktailDetailFragment;
import bii40t.mik.org.mixerv2.model.userCocktail.UserCocktail;
import bii40t.mik.org.mixerv2.model.userCocktail.UserCocktailService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UserCocktailRecyclerViewAdapter extends RecyclerView.Adapter<UserCocktailRecyclerViewAdapter.UserCocktailViewHolder> {

    private Context myContext;
    private List<UserCocktail> newCocktailsByUser;

    public UserCocktailRecyclerViewAdapter(Context myContext, List<UserCocktail> newCocktailsByUser) {
        this.myContext = myContext;
        this.newCocktailsByUser = newCocktailsByUser;
    }

    @NonNull
    @Override
    public UserCocktailViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater myInflater = LayoutInflater.from(myContext);
        View view = myInflater.inflate(R.layout.cardview_user_cocktail, viewGroup, false);
        return new UserCocktailViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final UserCocktailViewHolder myViewHolder, final int i) {
        myViewHolder.txtCoctailName.setText(newCocktailsByUser.get(i).getCocktailName());
        PicassoTrustAll.getInstance(myContext)
                .load(MainActivity.url + "image/" + newCocktailsByUser.get(i).getId())
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .into(myViewHolder.imgCoctailThumbNail);

        myViewHolder.cardView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Gson gson = new Gson();
                Bundle bundle = new Bundle();
                bundle.putBoolean("UserCocktail", true);
                bundle.putString("Cocktail", gson.toJson(newCocktailsByUser.get(i)));
                CocktailDetailFragment cocktailDetail = new CocktailDetailFragment();
                cocktailDetail.setArguments(bundle);
                AppCompatActivity activity = (AppCompatActivity) view.getContext();
                activity.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, cocktailDetail).addToBackStack(null).commit();
            }
        });

        myViewHolder.imgDelete.setVisibility(View.VISIBLE);
        myViewHolder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(myContext);
                alertDialog.setTitle(R.string.confirmDelete);
                alertDialog.setMessage(R.string.areYouSureDelete);
                alertDialog.setPositiveButton(R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                delete(newCocktailsByUser.get(i));
                                newCocktailsByUser.remove(i);
                                notifyItemRemoved(i);
                                notifyItemRangeChanged(i, newCocktailsByUser.size());
                                notifyDataSetChanged();
                            }
                        });
                alertDialog.setNegativeButton(R.string.no,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                alertDialog.show();
                notifyDataSetChanged();
            }
        });

    }

    private void delete(final UserCocktail userCocktail) {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(MainActivity.url).client(HttpsUtil.getOkHttpClient()
                .build()).addConverterFactory(GsonConverterFactory.create()).build();

        final UserCocktailService jsonPlaceholderApi = retrofit.create(UserCocktailService.class);
        Call<Void> call = jsonPlaceholderApi.deleteUserCocktail(userCocktail.getId());
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                deleteDromLocal(userCocktail);
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void deleteDromLocal(final UserCocktail userCocktail) {
        class DeleteCocktailFromLocal extends AsyncTask<Void, Void, Void>{
            @Override
            protected Void doInBackground(Void... voids) {
                DatabaseClient.getInstance(myContext).getMixerAppDatabase()
                        .userCocktailDao().delete(userCocktail);
                return null;
            }
        }
        DeleteCocktailFromLocal deleteCocktailFromLocal =new DeleteCocktailFromLocal();
        deleteCocktailFromLocal.execute();
    }

    @Override
    public int getItemCount() {
        return newCocktailsByUser.size();
    }

    public static class UserCocktailViewHolder extends RecyclerView.ViewHolder{

        TextView txtCoctailName;
        ImageView imgCoctailThumbNail, imgDelete;
        CardView cardView;

        public UserCocktailViewHolder(View itemView) {
            super(itemView);
            txtCoctailName = itemView.findViewById(R.id.txtCoctailName);
            imgCoctailThumbNail = itemView.findViewById(R.id.imgViewCoctail);
            cardView = itemView.findViewById(R.id.cardView);
            imgDelete = itemView.findViewById(R.id.imgDelete);
        }
    }
}
