package bii40t.mik.org.mixerv2.addNewCocktailFragment;

public interface AddNewCocktailFragmentInterface {

    void showErrorMessage(int id);

    void showMessage(int id);

    void showProgressDialog();

    void hideProgressDialog();

    void takePhoto();

    void openUserCocktailFragment();

    void getIngredient();

    void edit();
}