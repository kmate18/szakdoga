package bii40t.mik.org.mixerv2.favouriteCocktailFragment;

import java.util.List;

import bii40t.mik.org.mixerv2.model.cocktail.Cocktail;

public interface FavouriteCocktailFragmentInterface {
    void showProgressDialog();

    void hideProgressDialog();

    void setFavouriteRecycleViewAdapter(List<Cocktail> cocktails);

    void setTitle(List<Cocktail> favouritesCocktail);
}