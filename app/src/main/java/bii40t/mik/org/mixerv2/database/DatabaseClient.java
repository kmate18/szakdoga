package bii40t.mik.org.mixerv2.database;

import android.arch.persistence.room.Room;
import android.content.Context;

public class DatabaseClient {

    private static DatabaseClient mInstance;

    private MixerAppDatabase mixerAppDatabase;

    private DatabaseClient(Context mCtx) {
        mixerAppDatabase = Room.databaseBuilder(mCtx, MixerAppDatabase.class, "Mixer22").build();
    }

    public static synchronized DatabaseClient getInstance(Context mCtx) {
        if (mInstance == null) {
            mInstance = new DatabaseClient(mCtx);
        }
        return mInstance;
    }

    public MixerAppDatabase getMixerAppDatabase() {
        return mixerAppDatabase;
    }
}