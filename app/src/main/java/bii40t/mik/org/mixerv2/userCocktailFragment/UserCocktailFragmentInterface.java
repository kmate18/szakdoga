package bii40t.mik.org.mixerv2.userCocktailFragment;

import java.util.List;

import bii40t.mik.org.mixerv2.model.userCocktail.UserCocktail;

public interface UserCocktailFragmentInterface {

    void showProgressDialog();

    void hideProgressDialog();

    void setMyCocktailRecycleViewAdapter(List<UserCocktail> userCocktails);
}