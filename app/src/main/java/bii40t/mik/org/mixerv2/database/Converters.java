package bii40t.mik.org.mixerv2.database;

import android.arch.persistence.room.TypeConverter;

public class Converters {

    @TypeConverter
    public static String fromArrayLisr(char[] list) {
        String passWd = "";
        for (int i = 0; i < list.length; i++) {
            passWd += list[i];
        }
        return passWd;
    }

    @TypeConverter
    public static char[] stringToChar(String list) {
        return list.toCharArray();
    }
}
