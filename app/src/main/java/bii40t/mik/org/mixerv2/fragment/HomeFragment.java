package bii40t.mik.org.mixerv2.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import bii40t.mik.org.mixerv2.R;
import bii40t.mik.org.mixerv2.model.cocktail.Cocktail;
import bii40t.mik.org.mixerv2.model.recipe.Recipe;

public class HomeFragment extends BaseFragment {

    private static final String INGREDIENT_RUM = "rum";
    private static final String INGREDIENT_WHISKEY = "whiskey";
    private static final String INGREDIENT_VODKA = "vodka";
    private static final String INGREDIENT_GIN = "gin";

    private static final String CATEGORY_LONGDRINK = "Longdrink";
    private static final String CATEGORY_SHOOTER = "Shooter";
    private static final String CATEGORY_CREAMY = "Creamy";
    private static final String CATEGORY_SHORT = "Short";
    private static final String CATEGORY_FROZEN = "Frozen";
    private static final String CATEGORY_MARTINI = "Martini";
    private static final String CATEGORY_TROPICAL = "Tropical";
    private static final String CATEGORY_SPARKLING = "Sparkling";

    private TextView goToRum, goToWhiskey, goToVodka, goToGin;
    private TextView goToTropical, goToCreamy, goToShooter, goToLongdrink, goToShort, goToMartini, goToSparkling, goToFrozen;
    private View view;
    private List<Cocktail> cocktails;
    private boolean doubleBackToExitPressedOnce;

    public HomeFragment() {
    }

    @SuppressLint("ValidFragment")
    public HomeFragment(List<Cocktail> cocktails) {
        this.cocktails = cocktails;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);

        Gson gson = new Gson();
        String cocktailsString = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("Cocktails", "");
        cocktails = gson.fromJson(cocktailsString, new TypeToken<List<Cocktail>>() {
        }.getType());

        goToRum = view.findViewById(R.id.goToRum);
        goToGin = view.findViewById(R.id.goToGin);
        goToVodka = view.findViewById(R.id.goToVodka);
        goToWhiskey = view.findViewById(R.id.goToWhiskey);

        goToTropical = view.findViewById(R.id.goToTropical);
        goToCreamy = view.findViewById(R.id.goToCreamy);
        goToShooter = view.findViewById(R.id.goToShooter);
        goToLongdrink = view.findViewById(R.id.goToLongdrink);
        goToShort = view.findViewById(R.id.goToShort);
        goToMartini = view.findViewById(R.id.goToMartini);
        goToSparkling = view.findViewById(R.id.goToSparkling);
        goToFrozen = view.findViewById(R.id.goToFrozen);

        goToRum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToCocktailByIngredient(INGREDIENT_RUM);
            }
        });
        goToWhiskey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToCocktailByIngredient(INGREDIENT_WHISKEY);
            }
        });
        goToVodka.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToCocktailByIngredient(INGREDIENT_VODKA);
            }
        });
        goToGin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToCocktailByIngredient(INGREDIENT_GIN);
            }
        });

        goToLongdrink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToCocktailByCategory(CATEGORY_LONGDRINK);
            }
        });

        goToShooter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToCocktailByCategory(CATEGORY_SHOOTER);
            }
        });

        goToCreamy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToCocktailByCategory(CATEGORY_CREAMY);
            }
        });

        goToShort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToCocktailByCategory(CATEGORY_SHORT);
            }
        });

        goToFrozen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToCocktailByCategory(CATEGORY_FROZEN);
            }
        });

        goToMartini.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToCocktailByCategory(CATEGORY_MARTINI);
            }
        });

        goToTropical.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToCocktailByCategory(CATEGORY_TROPICAL);
            }
        });

        goToSparkling.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToCocktailByCategory(CATEGORY_SPARKLING);
            }
        });

        return view;
    }

    private void goToCocktailByCategory(String category) {
        List<Cocktail> cocktailsByCategory = new ArrayList<>();
        for (int i = 0; i < cocktails.size(); i++) {
            if (cocktails.get(i).getCategory().equals(category)) {
                if (!cocktailsByCategory.contains(cocktails.get(i))) {
                    cocktailsByCategory.add(cocktails.get(i));
                }
            }
        }
        openCategoryFragment(cocktailsByCategory);
    }

    private void goToCocktailByIngredient(String ingredient) {
        List<Cocktail> cocktailsByIngredient = new ArrayList<>();
        for (int i = 0; i < cocktails.size(); i++) {

            for (Recipe re : cocktails.get(i).getRecipes()) {
                if (re.getIngredient().getName().toLowerCase().contains(ingredient)
                        && re.getIngredient() != null) {
                    if (!cocktailsByIngredient.contains(cocktails.get(i))) {
                        cocktailsByIngredient.add(cocktails.get(i));
                    }
                }
            }
        }
        openCategoryFragment(cocktailsByIngredient);
    }

    private void openCategoryFragment(List<Cocktail> cocktails1) {
        Fragment fragment = new CategoryFragment(cocktails1);
        this.getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, fragment).commit();
    }

    @Override
    public boolean onBackPressed() {
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(getActivity(), R.string.doubleTapToExit, Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return false;
        } else {
            this.getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).commit();
            return true;
        }
    }
}