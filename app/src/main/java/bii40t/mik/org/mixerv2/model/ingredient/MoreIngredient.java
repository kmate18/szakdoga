package bii40t.mik.org.mixerv2.model.ingredient;

import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class MoreIngredient {

    private Spinner spinner;
    private TextView textView;
    private EditText editText;

    public MoreIngredient(Spinner spinner, TextView textView, EditText editText) {
        this.spinner = spinner;
        this.textView = textView;
        this.editText = editText;
    }

    public Spinner getSpinner() {
        return spinner;
    }

    public void setSpinner(Spinner spinner) {
        this.spinner = spinner;
    }

    public TextView getTextView() {
        return textView;
    }

    public void setTextView(TextView textView) {
        this.textView = textView;
    }

    public EditText getEditText() {
        return editText;
    }

    public void setEditText(EditText editText) {
        this.editText = editText;
    }
}
