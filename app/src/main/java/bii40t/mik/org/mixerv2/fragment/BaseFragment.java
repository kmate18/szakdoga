package bii40t.mik.org.mixerv2.fragment;

import android.support.v4.app.Fragment;

public class BaseFragment extends Fragment {

    public boolean onBackPressed() {
        return false;
    }
}