package bii40t.mik.org.mixerv2.favouriteCocktailFragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.List;

import bii40t.mik.org.mixerv2.HttpsUtil;
import bii40t.mik.org.mixerv2.fragment.BaseFragment;
import bii40t.mik.org.mixerv2.fragment.HomeFragment;
import bii40t.mik.org.mixerv2.model.cocktail.Cocktail;
import bii40t.mik.org.mixerv2.model.user.User;
import bii40t.mik.org.mixerv2.viewAdapter.FavouriteCocktailRecycleViewAdapter;
import bii40t.mik.org.mixerv2.R;

public class FavouriteCocktailFragment extends BaseFragment implements FavouriteCocktailFragmentInterface {

    private RecyclerView myView;
    private TextView textViewTitle;
    private SharedPreferences sharedPreferences;
    private ProgressDialog progressDialog;
    private User user;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        sharedPreferences = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String userString = sharedPreferences.getString("User", "");
        user = gson.fromJson(userString, User.class);
        FavouriteCocktailPresenter favouriteCocktailPresenter
                = new FavouriteCocktailPresenter(this,user,getContext());
        HttpsUtil httpsUtil = new HttpsUtil(getActivity());
        View view = inflater.inflate(R.layout.fragment_favourite_cocktail, container, false);
        myView = (RecyclerView) view.findViewById(R.id.coctailRecyclerView);

        if (httpsUtil.isOnline()) {
            favouriteCocktailPresenter.getFavouriteCocktailFromServer();
        } else {
            favouriteCocktailPresenter.getFavouriteCocktailFromLocalDatabase();
        }
        textViewTitle = view.findViewById(R.id.textViewFavouriteCoktail);
        return view;
    }

    @Override
    public void setTitle(List<Cocktail> favouriteCocktailList) {
        if (!favouriteCocktailList.isEmpty()) {
            textViewTitle.setText(R.string.favouriteCoktails);
        } else {
            textViewTitle.setText(R.string.noFavourite);
        }
    }

    @Override
    public void showProgressDialog() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
    }

    @Override
    public void hideProgressDialog() {
        progressDialog.dismiss();
    }

    @Override
    public void setFavouriteRecycleViewAdapter(List<Cocktail> favouriteCocktailList) {
        FavouriteCocktailRecycleViewAdapter myAdapter = new FavouriteCocktailRecycleViewAdapter(favouriteCocktailList, this.getContext());
        myView.setLayoutManager(new GridLayoutManager(this.getContext(), 1));
        myView.setAdapter(myAdapter);
    }

    @Override
    public boolean onBackPressed() {
        this.getActivity().getSupportFragmentManager().beginTransaction().
                replace(R.id.fragment_container, new HomeFragment()).commit();
        return true;
    }
}