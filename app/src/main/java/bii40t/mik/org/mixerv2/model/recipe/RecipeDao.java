package bii40t.mik.org.mixerv2.model.recipe;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;


@Dao
public interface RecipeDao {

    @Query("select * from recipes where cocktail_id =:id")
    public List<Recipe> findAllByCocktailId(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void save(Recipe recipe);

    @Query("delete from recipes where  cocktail_id = :cocktail_id")
    public void deleteRecipeByCocktailId(int cocktail_id);
}
