package bii40t.mik.org.mixerv2.model.ingredient;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface IngredientService {

    @GET("allingredient")
    Call<List<Ingredient>> getIngredients();
}
