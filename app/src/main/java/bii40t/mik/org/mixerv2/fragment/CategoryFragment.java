package bii40t.mik.org.mixerv2.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import bii40t.mik.org.mixerv2.R;
import bii40t.mik.org.mixerv2.model.cocktail.Cocktail;
import bii40t.mik.org.mixerv2.viewAdapter.CategoryRecyclerViewAdapter;

public class CategoryFragment extends BaseFragment {

    private List<Cocktail> cocktails;

    public CategoryFragment() {
    }

    @SuppressLint("ValidFragment")
    public CategoryFragment(List<Cocktail> cocktails) {
        this.cocktails = cocktails;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_alcohol_category, container, false);
        RecyclerView myView = (RecyclerView) view.findViewById(R.id.coctailRecyclerView);
        CategoryRecyclerViewAdapter myAdapter
                = new CategoryRecyclerViewAdapter(this.getContext(), cocktails);
        myView.setLayoutManager(new GridLayoutManager(this.getContext(), 2));
        myView.setAdapter(myAdapter);

        return view;
    }

    @Override
    public boolean onBackPressed() {
        this.getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, new HomeFragment()).commit();
        return true;
    }
}