package bii40t.mik.org.mixerv2.viewAdapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.List;

import bii40t.mik.org.mixerv2.mainactivity.MainActivity;
import bii40t.mik.org.mixerv2.PicassoTrustAll;
import bii40t.mik.org.mixerv2.R;
import bii40t.mik.org.mixerv2.model.cocktail.Cocktail;
import bii40t.mik.org.mixerv2.cocktailDetailFragment.CocktailDetailFragment;

public class FavouriteCocktailRecycleViewAdapter extends RecyclerView.Adapter<FavouriteCocktailRecycleViewAdapter.MyViewHolder> {

    private Context myContext;
    private List<Cocktail> favouriteCocktails;

    public FavouriteCocktailRecycleViewAdapter(List<Cocktail> favouriteCocktails, Context myContext) {
        this.favouriteCocktails = favouriteCocktails;
        this.myContext = myContext;
    }

    @NonNull
    @Override
    public FavouriteCocktailRecycleViewAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater myInflater = LayoutInflater.from(myContext);
        View view = myInflater.inflate(R.layout.cardview_favourite_cocktail, viewGroup, false);
        return new FavouriteCocktailRecycleViewAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FavouriteCocktailRecycleViewAdapter.MyViewHolder myViewHolder, final int i) {
        myViewHolder.txtCoctailName.setText(favouriteCocktails.get(i).getCocktailName());
        PicassoTrustAll.getInstance(myContext)
                .load(MainActivity.url + "image/" + favouriteCocktails.get(i).getId())
                .into(myViewHolder.imgCoctailThumbNail);

        myViewHolder.cardView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Gson gson = new Gson();
                String cocktail = gson.toJson(favouriteCocktails.get(i));
                Bundle bundle = new Bundle();
                bundle.putString("Cocktail", cocktail);
                CocktailDetailFragment cocktailDetail = new CocktailDetailFragment();
                cocktailDetail.setArguments(bundle);
                AppCompatActivity activity = (AppCompatActivity) view.getContext();
                activity.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, cocktailDetail).addToBackStack(null).commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return favouriteCocktails.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtCoctailName;
        ImageView imgCoctailThumbNail;
        CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);
            txtCoctailName = itemView.findViewById(R.id.txtCoctailName);
            imgCoctailThumbNail = itemView.findViewById(R.id.imgViewCoctail);
            cardView = itemView.findViewById(R.id.cardView);
        }
    }
}