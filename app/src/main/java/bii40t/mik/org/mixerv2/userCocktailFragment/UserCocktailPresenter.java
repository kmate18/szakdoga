package bii40t.mik.org.mixerv2.userCocktailFragment;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;

import java.util.ArrayList;
import java.util.List;

import bii40t.mik.org.mixerv2.ApiClient;
import bii40t.mik.org.mixerv2.database.DatabaseClient;
import bii40t.mik.org.mixerv2.model.recipe.Recipe;
import bii40t.mik.org.mixerv2.model.user.User;
import bii40t.mik.org.mixerv2.model.userCocktail.UserCocktail;
import bii40t.mik.org.mixerv2.model.userCocktail.UserCocktailService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserCocktailPresenter implements UserCocktailPresenterInterface {

    private UserCocktailFragmentInterface userCocktailFragmentInterface;
    private UserCocktailService userCocktailService = ApiClient.getClient().create(UserCocktailService.class);
    private User user;
    private Context context;

    public UserCocktailPresenter(UserCocktailFragmentInterface userCocktailFragmentInterface, User user, Context context) {
        this.userCocktailFragmentInterface = userCocktailFragmentInterface;
        this.user = user;
        this.context = context;
    }

    @Override
    public void getUserCocktailFromServer() {
        if (user != null) {
            userCocktailFragmentInterface.showProgressDialog();
            Call<List<UserCocktail>> call = userCocktailService.getUserCocktails(user.getUsername());
            call.enqueue(new Callback<List<UserCocktail>>() {
                @Override
                public void onResponse(Call<List<UserCocktail>> call, Response<List<UserCocktail>> response) {
                    List<UserCocktail> userCocktails = response.body();
                    userCocktailFragmentInterface.setMyCocktailRecycleViewAdapter(userCocktails);
                    userCocktailFragmentInterface.hideProgressDialog();
                    saveUserCocktail(userCocktails);
                }

                @Override
                public void onFailure(Call<List<UserCocktail>> call, Throwable t) {
                    t.printStackTrace();
                    userCocktailFragmentInterface.hideProgressDialog();
                }
            });
        }
    }

    @Override
    public void saveUserCocktail(final List<UserCocktail> userCocktails) {
        class SaveFavourite extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... voids) {
                List<UserCocktail> localUserCocktail = DatabaseClient.getInstance(context).getMixerAppDatabase()
                        .userCocktailDao()
                        .findAllByUserName(user.getUsername());

                for (int i = 0; i < userCocktails.size(); i++) {
                    userCocktails.get(i).setUser(user);
                    if (!localUserCocktail.contains(userCocktails.get(i))) {
                        DatabaseClient.getInstance(context).getMixerAppDatabase()
                                .userCocktailDao()
                                .save(userCocktails.get(i));

                        for (Recipe recipe : userCocktails.get(i).getRecipes()) {
                            recipe.setCocktail(userCocktails.get(i));
                            DatabaseClient.getInstance(context).getMixerAppDatabase()
                                    .recipeDao().save(recipe);
                        }
                    }
                }
                return null;
            }
        }
        SaveFavourite sf = new SaveFavourite();
        sf.execute();
    }

    @Override
    public void getUserCocktailFromLocalDatabase() {
        //userCocktailFragmentInterface.showProgressDialog();
        final List<UserCocktail> userCocktails = new ArrayList<>();
        class LoadUserCocktail extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... voids) {
                List<UserCocktail> localUserCocktail = DatabaseClient.getInstance(context).getMixerAppDatabase()
                        .userCocktailDao()
                        .findAllByUserName(user.getUsername());

                for (UserCocktail cocktail : localUserCocktail) {
                    List<Recipe> recipes = DatabaseClient.getInstance(context)
                            .getMixerAppDatabase().recipeDao()
                            .findAllByCocktailId(cocktail.getId());
                    cocktail.setRecipes(recipes);
                    userCocktails.add(cocktail);
                }
                Handler mHandler = new Handler(Looper.getMainLooper());
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        userCocktailFragmentInterface.setMyCocktailRecycleViewAdapter(userCocktails);
                    }
                });
               // userCocktailFragmentInterface.setMyCocktailRecycleViewAdapter(userCocktails);
               // userCocktailFragmentInterface.hideProgressDialog();
                return null;
            }
        }
        LoadUserCocktail loadUserCocktail = new LoadUserCocktail();
        loadUserCocktail.execute();
        //userCocktailFragmentInterface.setMyCocktailRecycleViewAdapter(userCocktails);
    }
}