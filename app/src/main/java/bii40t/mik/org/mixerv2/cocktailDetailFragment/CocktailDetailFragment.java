package bii40t.mik.org.mixerv2.cocktailDetailFragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import bii40t.mik.org.mixerv2.fragment.BaseFragment;
import bii40t.mik.org.mixerv2.mainactivity.MainActivity;
import bii40t.mik.org.mixerv2.PicassoTrustAll;
import bii40t.mik.org.mixerv2.R;
import bii40t.mik.org.mixerv2.model.cocktail.Cocktail;
import bii40t.mik.org.mixerv2.model.recipe.Recipe;
import bii40t.mik.org.mixerv2.model.userCocktail.UserCocktail;
import bii40t.mik.org.mixerv2.model.user.User;

public class CocktailDetailFragment extends BaseFragment implements CocktailDetailFragmentInterface {
    private TextView txtViewDescription, txtViewName, txtViewIngredients;
    private ImageView imageViewCocktail;
    private CheckBox chkBoxShare;
    private SharedPreferences sharedPreferences;
    private String cocktailJson;
    private Button btnEdit;
    private FloatingActionButton floatingActionButtonFavourite;
    private Handler mHandler;
    private String imageUrl;
    private Gson gson;
    private User user;
    private Cocktail cocktail;
    private CocktailDetailPresenter cocktailDetailPresenter;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_coctail_detail, container, false);
        txtViewDescription = view.findViewById(R.id.txtViewDetailsDescription);
        txtViewName = view.findViewById(R.id.txtViewDetailsCoctailName);
        txtViewIngredients = view.findViewById(R.id.txtViewDetailsIngredients);
        imageViewCocktail = view.findViewById(R.id.imgViewDetails);
        chkBoxShare = view.findViewById(R.id.checkBoxShare);
        btnEdit = view.findViewById(R.id.btnEdit);
        btnEdit.setVisibility(View.INVISIBLE);
        floatingActionButtonFavourite = view.findViewById(R.id.floatingActionButtonFavourite);
        floatingActionButtonFavourite.hide();

        mHandler = new Handler(Looper.getMainLooper());
        gson = new GsonBuilder().setLenient().create();
        sharedPreferences = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        cocktailJson = getArguments().getString("Cocktail");
        cocktail = gson.fromJson(cocktailJson, Cocktail.class);

        if (sharedPreferences.getBoolean("savelogin", false)) {
            setFloatingActionButtonForeground(R.drawable.ic_favourite_border);
            floatingActionButtonFavourite.show();
            String userString = sharedPreferences.getString("User", "");
            user = gson.fromJson(userString, User.class);
        }
        if (getArguments().getBoolean("UserCocktail")) {
            cocktail = gson.fromJson(cocktailJson, UserCocktail.class);
            if (((UserCocktail) cocktail).isShare()) {
                chkBoxShare.setChecked(true);
            }
            btnEdit.setVisibility(View.VISIBLE);
        } else {
            chkBoxShare.setVisibility(View.INVISIBLE);
        }

        cocktailDetailPresenter = new CocktailDetailPresenter(this,
                getContext(),user,cocktail) ;
        if (sharedPreferences.getBoolean("savelogin", false)) {
            cocktailDetailPresenter.getFavouriteCocktailFromServer();
        }
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cocktailDetailPresenter.editCocktail();
            }
        });
        floatingActionButtonFavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cocktailDetailPresenter.updateFavourite();
            }
        });
        chkBoxShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cocktailDetailPresenter.updateShare();
            }
        });

        imageUrl = getArguments().getString("Image");
        PicassoTrustAll.getInstance(getContext())
                .load(MainActivity.url + "image/" + cocktail.getId())
                .into(imageViewCocktail);

        String ingredient = "";
        for (Recipe recipe : cocktail.getRecipes()) {
            ingredient += recipe.getIngredient().getName() + ": " + recipe.getQuantity() + " " + recipe.getIngredient().getUnit() + "\n";
        }
        txtViewDescription.setText(cocktail.getDescription());
        txtViewIngredients.setText(ingredient);
        txtViewName.setText(cocktail.getCocktailName());
        return view;
    }

    @Override
    public boolean onBackPressed() {
        return super.onBackPressed();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void setFloatingActionButtonForeground(final int id) {
        mHandler.post(new Runnable() {
            public void run() {
                floatingActionButtonFavourite.setForeground(ContextCompat.getDrawable(getContext(), id));
            }
        });
    }

    @Override
    public void showMessage(int id) {
        Toast.makeText(getContext(),id,Toast.LENGTH_LONG).show();
    }
}