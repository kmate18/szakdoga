package bii40t.mik.org.mixerv2.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;

import bii40t.mik.org.mixerv2.HttpsUtil;
import bii40t.mik.org.mixerv2.mainactivity.MainActivity;
import bii40t.mik.org.mixerv2.R;
import bii40t.mik.org.mixerv2.model.user.User;

public class LogInActivity extends AppCompatActivity implements LogInViewInterface {

    private EditText editTextUserName, editTextPassword;
    private Button btnSignIn;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor sEditor;
    private LogInPresenterInterface logInPresenterInterface;
    private HttpsUtil httpsUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        editTextUserName = findViewById(R.id.editTextUserNameSignIn);
        editTextPassword = findViewById(R.id.editTextPasswordSignIn);
        btnSignIn = findViewById(R.id.btnSignIn);
        sharedPreferences = getSharedPreferences("login", MODE_PRIVATE);
        sEditor = sharedPreferences.edit();
        logInPresenterInterface = new LogInPresenter(this);
        httpsUtil = new HttpsUtil(getBaseContext());

        LinearLayout linearLayout = findViewById(R.id.linearLayoutActivitySignIn);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                View focusedView = getCurrentFocus();
                if (focusedView != null) {
                    inputMethodManager.hideSoftInputFromWindow(focusedView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                }
            }
        });

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(httpsUtil.isOnline()){
                    logInPresenterInterface.checkLoginDetail(editTextUserName.getText().toString(),
                            editTextPassword.getText().toString().toCharArray());
                }
            }
        });
    }

    @Override
    public void showErrorMessage(int id) {
        Toast.makeText(this, id, Toast.LENGTH_LONG).show();
    }

    @Override
    public void startActivity(User user) {
        Gson gson = new Gson();
        String json = gson.toJson(user);
        sEditor.putBoolean("savelogin", true);
        sEditor.putString("User", json);
        sEditor.commit();
        startActivity(new Intent(getApplication(), MainActivity.class));
    }

    @Override
    public void showMessage(int id) {
        Toast.makeText(this, id, Toast.LENGTH_LONG).show();
    }
}