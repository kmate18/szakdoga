package bii40t.mik.org.mixerv2.cocktailDetailFragment;

public interface CocktailDetailFragmentInterface {

    void setFloatingActionButtonForeground(int id);

    void showMessage(int id);
}
