package bii40t.mik.org.mixerv2.model.userCocktail;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface UserCocktailDao {

    @Query("select * from userCocktails where userCocktails.username =:userName")
    public List<UserCocktail> findAllByUserName(String userName);

    @Query("select * from userCocktails where userCocktails.cocktail_id =:id")
    public UserCocktail findById(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public long save(UserCocktail userCocktail);

    @Update
    public void update(UserCocktail userCocktail);

    @Delete
    public void delete(UserCocktail userCocktail);
}
