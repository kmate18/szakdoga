package bii40t.mik.org.mixerv2.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import bii40t.mik.org.mixerv2.R;

public class SettingsFragment extends Fragment {

    private Spinner languageSpinner;
    private SharedPreferences sharedPreferencesLanguage;
    private SharedPreferences.Editor sEditor;
    private List<String> languages = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        languageSpinner = view.findViewById(R.id.languageSpinner);
        languages.add("Chose");
        languages.add("English");
        languages.add("Hungarian");

        ArrayAdapter<String> languageAdapter = new ArrayAdapter<String>(this.getContext(),
                R.layout.support_simple_spinner_dropdown_item, languages);
        languageAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        languageSpinner.setAdapter(languageAdapter);

        sharedPreferencesLanguage = getActivity().getSharedPreferences("language", Context.MODE_PRIVATE);
        sEditor = sharedPreferencesLanguage.edit();

        languageSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                switch (i) {
                    case 1:
                        setLanguage("language", "en");
                        break;
                    case 2:
                        setLanguage("language", "hu");
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        return view;
    }

    private void setLanguage(String type, String language) {
        sEditor.putString(type, language);
        sEditor.commit();
        Intent intent = getActivity().getIntent();
        getActivity().finish();
        startActivity(intent);
    }
}
