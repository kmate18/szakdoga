package bii40t.mik.org.mixerv2.cocktailDetailFragment;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;

import com.google.gson.Gson;

import java.util.List;

import bii40t.mik.org.mixerv2.ApiClient;
import bii40t.mik.org.mixerv2.HttpsUtil;
import bii40t.mik.org.mixerv2.R;
import bii40t.mik.org.mixerv2.model.cocktail.Cocktail;
import bii40t.mik.org.mixerv2.database.DatabaseClient;
import bii40t.mik.org.mixerv2.model.favorite.FavouriteCocktail;
import bii40t.mik.org.mixerv2.model.favorite.FavouriteCocktailService;
import bii40t.mik.org.mixerv2.addNewCocktailFragment.AddNewCocktailFragment;
import bii40t.mik.org.mixerv2.model.user.User;
import bii40t.mik.org.mixerv2.model.userCocktail.UserCocktail;
import bii40t.mik.org.mixerv2.model.userCocktail.UserCocktailService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CocktailDetailPresenter implements CocktailDetailPresenterInterface {

    private CocktailDetailFragmentInterface cocktailDetailFragmentInterface;
    private FavouriteCocktailService favouriteCocktailService = ApiClient.getClient().create(FavouriteCocktailService.class);
    private UserCocktailService userCocktailService = ApiClient.getClient().create(UserCocktailService.class);
    private Context context;
    private User user;
    private HttpsUtil httpsUtil;
    private boolean isFavourite = false;
    private Cocktail cocktail;

    public CocktailDetailPresenter(CocktailDetailFragmentInterface cocktailDetailFragmentInterface, Context context, User user, Cocktail cocktail) {
        this.cocktailDetailFragmentInterface = cocktailDetailFragmentInterface;
        this.context = context;
        this.user = user;
        this.cocktail = cocktail;
        httpsUtil = new HttpsUtil(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void updateFavourite() {
        if (httpsUtil.isOnline()) {
            if (!isFavourite) {
                new Thread() {
                    public void run() {
                        try {
                            cocktailDetailFragmentInterface.setFloatingActionButtonForeground(R.drawable.ic_favourite_fill);
                            saveFavourite();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }.start();
            } else {
                new Thread() {
                    public void run() {
                        try {
                            cocktailDetailFragmentInterface.setFloatingActionButtonForeground(R.drawable.ic_favourite_border);
                            deleteFavourite();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }.start();
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void saveFavourite() {
        Call<Void> fav = favouriteCocktailService.saveFavourites(new FavouriteCocktail(cocktail.getId(),
                user.getUsername() + cocktail.getId(), user));
        fav.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                updateFavouriteStatusInLocalDatabase();
                cocktailDetailFragmentInterface.showMessage(R.string.saveFavourite);
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                t.printStackTrace();
            }
        });
        isFavourite = true;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void deleteFavourite() {
        Call<Void> fav = favouriteCocktailService.deleteFavourites(user.getUsername() + cocktail.getId());
        fav.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                updateFavouriteStatusInLocalDatabase();
                cocktailDetailFragmentInterface.showMessage(R.string.deleteFavourite);
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                t.printStackTrace();
            }
        });
        isFavourite = false;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void updateFavouriteStatusInLocalDatabase() {
        class UpdataFavouriteStatusInLocalDatabase extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... voids) {
                if (isFavourite) {
                    DatabaseClient.getInstance(context).getMixerAppDatabase()
                            .favouriteCocktailDao()
                            .save(new FavouriteCocktail(cocktail.getId(), user.getUsername() + cocktail.getId(), user));
                } else {
                    DatabaseClient.getInstance(context).getMixerAppDatabase()
                            .favouriteCocktailDao()
                            .deleteFavouriteCocktailById(user.getUsername() + cocktail.getId());
                }
                return null;
            }
        }
        UpdataFavouriteStatusInLocalDatabase updataFavouriteStatusInLocalDatabase = new UpdataFavouriteStatusInLocalDatabase();
        updataFavouriteStatusInLocalDatabase.execute();
    }

    @Override
    public void updateShare() {
        if (httpsUtil.isOnline()) {
            new Thread() {
                public void run() {
                    try {
                        Call<String> call = userCocktailService.shareCocktail(cocktail.getId());
                        call.enqueue(new Callback<String>() {
                            @Override
                            public void onResponse(Call<String> call, Response<String> response) {
                                if (((UserCocktail) cocktail).isShare()) {
                                    updateShareStatusInLocalDatabase();
                                    cocktailDetailFragmentInterface.showMessage(R.string.unShared);
                                } else {
                                    updateShareStatusInLocalDatabase();
                                    cocktailDetailFragmentInterface.showMessage(R.string.share);
                                }
                            }

                            @Override
                            public void onFailure(Call<String> call, Throwable t) {
                                t.printStackTrace();
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.start();
        }
    }

    @Override
    public void updateShareStatusInLocalDatabase() {
        class UpdateShare extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... voids) {
                UserCocktail userCocktail = DatabaseClient.getInstance(context).getMixerAppDatabase()
                        .userCocktailDao()
                        .findById(cocktail.getId());

                if (userCocktail.isShare()) {
                    userCocktail.setShare(false);
                    DatabaseClient.getInstance(context).getMixerAppDatabase().userCocktailDao()
                            .update(userCocktail);
                } else {
                    userCocktail.setShare(true);
                    DatabaseClient.getInstance(context).getMixerAppDatabase().userCocktailDao()
                            .update(userCocktail);
                }
                return null;
            }
        }
        UpdateShare update = new UpdateShare();
        update.execute();
    }

    @Override
    public void editCocktail() {
        Gson gson = new Gson();
        String cocktailJson = gson.toJson(cocktail);
        Bundle bundle = new Bundle();
        bundle.putBoolean("Edit", true);
        bundle.putString("EditCocktail", cocktailJson);
        AddNewCocktailFragment editCocktail = new AddNewCocktailFragment();
        editCocktail.setArguments(bundle);
        AppCompatActivity activity = (AppCompatActivity) context;
        activity.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, editCocktail).commit();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void getFavouriteCocktailFromServer() {
        Call<List<FavouriteCocktail>> listCall = favouriteCocktailService.getFavourites(user.getUsername());
        listCall.enqueue(new Callback<List<FavouriteCocktail>>() {
            @Override
            public void onResponse(Call<List<FavouriteCocktail>> call, Response<List<FavouriteCocktail>> response) {
                List<FavouriteCocktail> favouriteCocktails = response.body();
                for (int i = 0; i < favouriteCocktails.size(); i++) {
                    if (favouriteCocktails.get(i).getCocktailId() == cocktail.getId()) {
                        cocktailDetailFragmentInterface.setFloatingActionButtonForeground(R.drawable.ic_favourite_fill);
                        isFavourite = true;
                        break;
                    }
                }
            }

            @Override
            public void onFailure(Call<List<FavouriteCocktail>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}