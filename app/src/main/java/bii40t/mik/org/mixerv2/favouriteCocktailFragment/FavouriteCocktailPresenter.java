package bii40t.mik.org.mixerv2.favouriteCocktailFragment;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import bii40t.mik.org.mixerv2.ApiClient;
import bii40t.mik.org.mixerv2.database.DatabaseClient;

import bii40t.mik.org.mixerv2.model.cocktail.Cocktail;
import bii40t.mik.org.mixerv2.model.favorite.FavouriteCocktail;
import bii40t.mik.org.mixerv2.model.favorite.FavouriteCocktailService;
import bii40t.mik.org.mixerv2.model.user.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FavouriteCocktailPresenter implements FavouriteCocktailPresenterInterface {

    private FavouriteCocktailFragmentInterface favouriteCocktailFragmentInterface;
    private List<Cocktail> cocktails;
    private FavouriteCocktailService favouriteCocktailService = ApiClient.getClient().create(FavouriteCocktailService.class);
    private User user;
    private Context context;

    public FavouriteCocktailPresenter(FavouriteCocktailFragmentInterface favouriteCocktailFragmentInterface, User user, Context context) {
        this.favouriteCocktailFragmentInterface = favouriteCocktailFragmentInterface;
        this.user = user;
        this.context = context;
        Gson gson = new Gson();
        String cocktailsString = PreferenceManager.getDefaultSharedPreferences(context).getString("Cocktails", "");
        cocktails = gson.fromJson(cocktailsString, new TypeToken<List<Cocktail>>() {}.getType());
    }

    @Override
    public void getFavouriteCocktailFromServer() {
        favouriteCocktailFragmentInterface.showProgressDialog();
        Call<List<FavouriteCocktail>> listCall = favouriteCocktailService.getFavourites(user.getUsername());
        listCall.enqueue(new Callback<List<FavouriteCocktail>>() {
            @Override
            public void onResponse(Call<List<FavouriteCocktail>> call, Response<List<FavouriteCocktail>> response) {
                List<FavouriteCocktail> favouriteCocktails = response.body();
                List<Cocktail> favouritesCocktail = new ArrayList<>();

                for (int i = 0; i < favouriteCocktails.size(); i++) {
                    for (int j = 0; j < cocktails.size(); j++) {
                        if (favouriteCocktails.get(i).getCocktailId() == cocktails.get(j).getId()) {
                            favouritesCocktail.add(cocktails.get(j));
                            break;
                        }
                    }
                }
                favouriteCocktailFragmentInterface.setFavouriteRecycleViewAdapter(favouritesCocktail);
                favouriteCocktailFragmentInterface.setTitle(favouritesCocktail);
                favouriteCocktailFragmentInterface.hideProgressDialog();
                saveFavouriteCocktail(favouriteCocktails);
            }

            @Override
            public void onFailure(Call<List<FavouriteCocktail>> call, Throwable t) {
                t.printStackTrace();
                favouriteCocktailFragmentInterface.hideProgressDialog();
            }
        });
    }

    @Override
    public void saveFavouriteCocktail(final List<FavouriteCocktail> favouriteCocktails) {
        class SaveFavourite extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... voids) {
                List<FavouriteCocktail> localFavourites = DatabaseClient.getInstance(context).getMixerAppDatabase()
                        .favouriteCocktailDao()
                        .findAllFavouriteCocktailByUserName(user.getUsername());

                for (int i = 0; i < favouriteCocktails.size(); i++) {
                    favouriteCocktails.get(i).setUser(user);
                    if (!localFavourites.contains(favouriteCocktails.get(i))) {
                        DatabaseClient.getInstance(context).getMixerAppDatabase()
                                .favouriteCocktailDao()
                                .save(favouriteCocktails.get(i));
                    }
                }
                return null;
            }
        }
        SaveFavourite sf = new SaveFavourite();
        sf.execute();
    }

    @Override
    public void getFavouriteCocktailFromLocalDatabase() {
       // favouriteCocktailFragmentInterface.showProgressDialog();
      final  List<Cocktail> favouriteCocktailList = new ArrayList<>();
        class LoadFavourite extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... voids) {
                List<FavouriteCocktail> localFavourites = DatabaseClient.getInstance(context).getMixerAppDatabase()
                        .favouriteCocktailDao()
                        .findAllFavouriteCocktailByUserName(user.getUsername());

                for (int i = 0; i < localFavourites.size(); i++) {
                    for (int j = 0; j < cocktails.size(); j++) {
                        if (localFavourites.get(i).getCocktailId() == cocktails.get(j).getId()) {
                            favouriteCocktailList.add(cocktails.get(j));
                            break;
                        }
                    }
                }
                Handler mHandler = new Handler(Looper.getMainLooper());
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        favouriteCocktailFragmentInterface.setFavouriteRecycleViewAdapter(favouriteCocktailList);
                    }
                });

                favouriteCocktailFragmentInterface.setTitle(favouriteCocktailList);
                //favouriteCocktailFragmentInterface.hideProgressDialog();
                return null;
            }
        }
        LoadFavourite loadFavourite = new LoadFavourite();
        loadFavourite.execute();
    }

}
