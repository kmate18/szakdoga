package bii40t.mik.org.mixerv2.mainactivity;

import android.support.v4.app.Fragment;
import android.view.MenuItem;

import java.util.List;

import bii40t.mik.org.mixerv2.model.cocktail.Cocktail;

public interface MainActivityViewInterface {

    void settings(MenuItem item);

    void logOut(MenuItem item);

    void logIn(MenuItem item);

    void signUp(MenuItem item);

    void hideListView();

    boolean loadFragment(Fragment fragment);

    void showProgressDialog();

    void hideProgressDialog();

    void searchList(List<Cocktail> cocktails);

    void addCocktailToPreferenciaManager(List<Cocktail> cocktails);
}
