package bii40t.mik.org.mixerv2.model.ingredient;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import java.util.List;
import java.util.Objects;

import bii40t.mik.org.mixerv2.model.recipe.Recipe;


@Entity(tableName = Ingredient.TBL_INGREDIENT)
public class Ingredient {

    public static final String TBL_INGREDIENT = "ingredients";
    public static final String COL_INGREDIENT_ID = "ingredient_id";
    public static final String COL_NAME = "name";
    public static final String COL_UNIT = "unit";

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = COL_INGREDIENT_ID)
    private Integer id;

    @ColumnInfo(name = COL_NAME)
    private String name;

    @ColumnInfo(name = COL_UNIT)
    private String unit;

    public Ingredient(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String ingredientName) {
        this.name = ingredientName;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ingredient that = (Ingredient) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(unit, that.unit);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, unit);
    }
}