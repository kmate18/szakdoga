package bii40t.mik.org.mixerv2;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    public static  String BASE_URL = "https://35.240.123.252:8443";         //35.240.123.252 - szerver ,80.252.50.208 - zoli otthoni
    private static Retrofit retrofit = null;
    private static Gson gson = new GsonBuilder().setLenient().create();

    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(HttpsUtil.getOkHttpClient().build())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit;
    }
}
