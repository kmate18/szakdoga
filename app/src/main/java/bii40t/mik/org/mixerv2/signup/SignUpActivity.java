package bii40t.mik.org.mixerv2.signup;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;

import bii40t.mik.org.mixerv2.HttpsUtil;
import bii40t.mik.org.mixerv2.mainactivity.MainActivity;
import bii40t.mik.org.mixerv2.R;
import bii40t.mik.org.mixerv2.model.user.User;

public class SignUpActivity extends AppCompatActivity implements SignUpViewInterface {

    private EditText editTextUserName, editTextPassword, editTextPasswordAgain;
    private Button btnSignUp;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor sEditor;
    private HttpsUtil httpsUtil;
    private SignUpPresenter signUpPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        httpsUtil = new HttpsUtil(getBaseContext());
        editTextUserName = findViewById(R.id.editTextuserNameSignUp);
        editTextPassword = findViewById(R.id.editTextPasswordSignUp);
        editTextPasswordAgain = findViewById(R.id.editTextPasswordSignUpAgain);

        btnSignUp = findViewById(R.id.btnSignUp);
        sharedPreferences = getSharedPreferences("login", MODE_PRIVATE);
        sEditor = sharedPreferences.edit();

        signUpPresenter = new SignUpPresenter(this);

        ConstraintLayout constraintLayout = findViewById(R.id.signUpConstraint);
        constraintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                View focusedView = getCurrentFocus();
                if (focusedView != null) {
                    inputMethodManager.hideSoftInputFromWindow(focusedView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                }
            }
        });
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (httpsUtil.isOnline()) {
                    signUpPresenter.checkSignUpDetails(editTextUserName.getText().toString(),
                            editTextPassword.getText().toString(),
                            editTextPasswordAgain.getText().toString());
                }
            }
        });
    }

    @Override
    public void showErrorMessage(int id) {
        Toast.makeText(this, id, Toast.LENGTH_LONG).show();
    }

    @Override
    public void startActivity(User user) {
        Gson gson = new Gson();
        String json = gson.toJson(user);
        sEditor.putBoolean("savelogin", true);
        sEditor.putString("User", json);
        sEditor.commit();
        startActivity(new Intent(getApplication(), MainActivity.class));
    }
}