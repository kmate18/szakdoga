package bii40t.mik.org.mixerv2.model.userCocktail;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;

import java.util.List;

import bii40t.mik.org.mixerv2.model.cocktail.Cocktail;
import bii40t.mik.org.mixerv2.model.recipe.Recipe;
import bii40t.mik.org.mixerv2.model.user.User;

@Entity(tableName = UserCocktail.TBL_USER_COCKTAIL)
public class UserCocktail extends Cocktail {

    public static final String TBL_USER_COCKTAIL = "userCocktails";
    public static final String COL_SHARE = "share";

    @ColumnInfo(name = COL_SHARE)
    private boolean share;

    @Embedded
    private User user;

    public UserCocktail(String cocktailName, String category, String description, String imageUrl, boolean share, User user) {
        super(cocktailName, category, description, imageUrl);
        this.share = share;
        this.user = user;
    }

    @Ignore
    public UserCocktail(String cocktailName, String category, String description, String imageUrl, List<Recipe> recipes, boolean share, User user) {
        super(cocktailName, category, description, imageUrl, recipes);
        this.share = share;
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isShare() {
        return share;
    }

    public void setShare(boolean share) {
        this.share = share;
    }
}