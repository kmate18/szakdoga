package bii40t.mik.org.mixerv2.model.cocktail;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import java.util.List;
import java.util.Objects;

import bii40t.mik.org.mixerv2.model.recipe.Recipe;

@Entity(tableName = Cocktail.TBL_COCKTAIL)
public class Cocktail {

    public static final String TBL_COCKTAIL = "cocktails";
    public static final String COL_ID = "cocktail_id";
    public static final String COL_COCKTAIL_NAME = "cocktail_name";
    public static final String COL_CATEGORY = "category";
    public static final String COL_DESCRIPTION = "description";
    public static final String COL_IMAGE_URL = "image_url";

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = COL_ID)
    private Integer id;

    @ColumnInfo(name = COL_COCKTAIL_NAME)
    private String cocktailName;

    @ColumnInfo(name = COL_CATEGORY)
    private String category;

    @ColumnInfo(name = COL_DESCRIPTION)
    private String description;

    @ColumnInfo(name = COL_IMAGE_URL)
    private String imageUrl;

    @Ignore
    private  List<Recipe> recipes;

    @Ignore
    public Cocktail(String cocktailName, String category, String description, String imageUrl,
                    List<Recipe> recipes) {
        this.cocktailName = cocktailName;
        this.category = category;
        this.description = description;
        this.imageUrl = imageUrl;
        this.recipes = recipes;
    }

    public Cocktail(String cocktailName, String category, String description, String imageUrl) {
        this.cocktailName = cocktailName;
        this.category = category;
        this.description = description;
        this.imageUrl = imageUrl;
    }

    public Integer getId() {
        return id;
    }

    public String getCocktailName() {
        return cocktailName;
    }

    public String getCategory() {
        return category;
    }

    public String getDescription() {
        return description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setCocktailName(String cocktailName) {
        this.cocktailName = cocktailName;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public List<Recipe> getRecipes() {
        return recipes;
    }

    public void setRecipes(List<Recipe> recipes) {
        this.recipes = recipes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cocktail cocktail = (Cocktail) o;
        return Objects.equals(id, cocktail.id);
                /*Objects.equals(category, cocktail.category) &&
                Objects.equals(description, cocktail.description) &&
                Objects.equals(imageUrl, cocktail.imageUrl);*/
    }

    @Override
    public int hashCode() {
        return Objects.hash(id/*cocktailName, category, description, imageUrl*/);
    }
}
