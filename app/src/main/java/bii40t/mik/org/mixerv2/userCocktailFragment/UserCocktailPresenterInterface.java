package bii40t.mik.org.mixerv2.userCocktailFragment;

import java.util.List;

import bii40t.mik.org.mixerv2.model.userCocktail.UserCocktail;

public interface UserCocktailPresenterInterface {

    void getUserCocktailFromServer();

    void saveUserCocktail(List<UserCocktail> userCocktails);

    void getUserCocktailFromLocalDatabase();
}