package bii40t.mik.org.mixerv2.model.recipe;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import bii40t.mik.org.mixerv2.model.cocktail.Cocktail;
import bii40t.mik.org.mixerv2.model.ingredient.Ingredient;

@Entity(tableName = Recipe.TBL_RECIPE)
public class Recipe {

    public static final String TBL_RECIPE = "recipes";
    public static final String COL_QUANTITY = "quantity";

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = COL_QUANTITY)
    private float quantity;

    @Embedded
    private Cocktail cocktail;

    @Embedded
    private Ingredient ingredient;

    public Recipe() {
    }

    public Recipe(float quantity, Ingredient ingredient) {
        this.quantity = quantity;
        this.ingredient = ingredient;
    }

    public Recipe(float quantity, Cocktail cocktail, Ingredient ingredient) {
        this.quantity = quantity;
        this.cocktail = cocktail;
        this.ingredient = ingredient;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getQuantity() {
        return quantity;
    }

    public void setQuantity(float quantity) {
        this.quantity = quantity;
    }

    public Cocktail getCocktail() {
        return cocktail;
    }

    public void setCocktail(Cocktail cocktail) {
        this.cocktail = cocktail;
    }

    public Ingredient getIngredient() {
        return ingredient;
    }

    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
    }
}
