package bii40t.mik.org.mixerv2.model.user;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = User.TBL_USER)
public class User {

    public static final String TBL_USER = "users";
    public static final String COL_USER_NAME = "username";
    public static final String COL_PASSWORD = "passWd";

    @PrimaryKey(autoGenerate = true)
    private Integer id;

    @ColumnInfo(name = COL_USER_NAME)
    private String username;

    @ColumnInfo(name = COL_PASSWORD)
    private char[] password;

    public User(String username, char[] password) {
        this.username = username;
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public char[] getPassword() {
        return password;
    }

    public void setPassword(char[] password) {
        this.password = password;
    }

    public int isValidData(){
        if(getUsername().equals("")){
            return 0;
        } else if(getPassword().length < 8 && getPassword().length >= 1){
            return 1;
        }else if(getPassword().length == 0){
            return 2;
        }else{
            return -1;
        }
    }
}
