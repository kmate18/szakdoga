package bii40t.mik.org.mixerv2.cocktailDetailFragment;

public interface CocktailDetailPresenterInterface {
    void updateFavourite();

    void saveFavourite();

    void deleteFavourite();

    void updateFavouriteStatusInLocalDatabase();

    void updateShare();

    void updateShareStatusInLocalDatabase();

    void editCocktail();

    void getFavouriteCocktailFromServer();
}
