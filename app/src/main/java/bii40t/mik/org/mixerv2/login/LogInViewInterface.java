package bii40t.mik.org.mixerv2.login;

import bii40t.mik.org.mixerv2.model.user.User;

public interface LogInViewInterface {
    void showErrorMessage(int id);
    void startActivity(User user);
    void showMessage(int id);
}
