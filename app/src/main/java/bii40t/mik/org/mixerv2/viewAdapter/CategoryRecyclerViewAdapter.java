package bii40t.mik.org.mixerv2.viewAdapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import java.util.List;

import bii40t.mik.org.mixerv2.mainactivity.MainActivity;
import bii40t.mik.org.mixerv2.PicassoTrustAll;
import bii40t.mik.org.mixerv2.R;
import bii40t.mik.org.mixerv2.model.cocktail.Cocktail;
import bii40t.mik.org.mixerv2.cocktailDetailFragment.CocktailDetailFragment;

public class CategoryRecyclerViewAdapter extends RecyclerView.Adapter<CategoryRecyclerViewAdapter.CategoryViewHolder> {

    private Context myContext;
    private List<Cocktail> coctails;

    public CategoryRecyclerViewAdapter(Context myContext, List<Cocktail> coctails) {
        this.myContext = myContext;
        this.coctails = coctails;
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater myInflater = LayoutInflater.from(myContext);
        View view = myInflater.inflate(R.layout.cardview_coctail,viewGroup, false);
        return new CategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CategoryViewHolder myViewHolder, final int i) {
        myViewHolder.txtCoctailName.setText(coctails.get(i).getCocktailName());
        PicassoTrustAll.getInstance(myContext)
                .load(MainActivity.url+"image/"+coctails.get(i).getId())
                .into(myViewHolder.imgCoctailThumbNail);

        myViewHolder.cardView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Gson gson = new Gson();
                Bundle bundle = new Bundle();
                bundle.putString("Cocktail", gson.toJson(coctails.get(i)));
                CocktailDetailFragment cocktailDetail = new CocktailDetailFragment();
                cocktailDetail.setArguments(bundle);
                AppCompatActivity activity = (AppCompatActivity) view.getContext();
                activity.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, cocktailDetail).addToBackStack(null).commit();

            }
        });
    }

    @Override
    public int getItemCount() {
        return coctails.size();
    }

    public static class CategoryViewHolder extends RecyclerView.ViewHolder{

        TextView txtCoctailName, txtUserName;
        ImageView imgCoctailThumbNail;
        CardView cardView;

        public CategoryViewHolder(View itemView) {
            super(itemView);
            txtCoctailName = itemView.findViewById(R.id.txtCoctailName);
            imgCoctailThumbNail = itemView.findViewById(R.id.imgViewCoctail);
            cardView = itemView.findViewById(R.id.cardView);
            txtUserName = itemView.findViewById(R.id.txtUserName);
            txtUserName.setVisibility(View.INVISIBLE);
        }
    }
}
