package bii40t.mik.org.mixerv2.login;

import bii40t.mik.org.mixerv2.ApiClient;
import bii40t.mik.org.mixerv2.AESCrypt;
import bii40t.mik.org.mixerv2.R;
import bii40t.mik.org.mixerv2.model.user.User;
import bii40t.mik.org.mixerv2.model.user.UserService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LogInPresenter implements LogInPresenterInterface {
    private LogInViewInterface logInViewInterface;
    private UserService userService = ApiClient.getClient().create(UserService.class);
    private AESCrypt AESCrypt = new AESCrypt();
    private int isValidLoginData;

    public LogInPresenter(LogInViewInterface logInViewInterface) {
        this.logInViewInterface = logInViewInterface;
    }

    @Override
    public void checkLoginDetail(String userName, char[] password) {
        User user = new User(userName, password);
        isValidLoginData = user.isValidData();
        if (isValidLoginData == 0) {
            logInViewInterface.showErrorMessage(R.string.emptyUserName);
        } else if (isValidLoginData == 1) {
            logInViewInterface.showErrorMessage(R.string.shortPasswd);
        } else if (isValidLoginData == 2) {
            logInViewInterface.showErrorMessage(R.string.emptyPassWd);
        } else {
            user.setPassword(AESCrypt.encrypt(new String(password)).toCharArray());
            login(user);
        }
    }

    @Override
    public void login(final User user) {
        Call<String> call = userService.login(user);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                String data = response.body();
                if (data != null && response != null) {
                    if (data.equals("Incorrect")) {
                        logInViewInterface.showMessage(R.string.signInIncorrect);
                    } else {
                        logInViewInterface.startActivity(user);
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}
