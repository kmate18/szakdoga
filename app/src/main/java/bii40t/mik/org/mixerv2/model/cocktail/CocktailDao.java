package bii40t.mik.org.mixerv2.model.cocktail;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface CocktailDao {

    @Query("select * from cocktails")
    public List<Cocktail> findAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void save(Cocktail cocktail);

    @Update
    public void update(Cocktail cocktail);
}
