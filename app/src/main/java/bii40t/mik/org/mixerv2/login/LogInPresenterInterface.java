package bii40t.mik.org.mixerv2.login;

import bii40t.mik.org.mixerv2.model.user.User;

public interface LogInPresenterInterface {
    void checkLoginDetail(String userName, char[] password);

    void login(User user);
}
