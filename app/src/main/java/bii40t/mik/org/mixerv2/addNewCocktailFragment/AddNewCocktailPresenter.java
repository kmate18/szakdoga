package bii40t.mik.org.mixerv2.addNewCocktailFragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.text.InputType;
import android.util.TypedValue;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import bii40t.mik.org.mixerv2.ApiClient;
import bii40t.mik.org.mixerv2.R;
import bii40t.mik.org.mixerv2.database.DatabaseClient;

import bii40t.mik.org.mixerv2.model.ingredient.Ingredient;
import bii40t.mik.org.mixerv2.model.ingredient.MoreIngredient;
import bii40t.mik.org.mixerv2.model.recipe.Recipe;
import bii40t.mik.org.mixerv2.model.user.User;
import bii40t.mik.org.mixerv2.model.userCocktail.UserCocktail;
import bii40t.mik.org.mixerv2.model.userCocktail.UserCocktailService;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddNewCocktailPresenter implements AddNewCocktailPresenterInterface {

    private UserCocktailService userCocktailService = ApiClient.getClient().create(UserCocktailService.class);
    private AddNewCocktailFragmentInterface addNewCocktailFragmentInterface;
    private Context context;
    private Integer cocktailId;
    private List<MoreIngredient> moreIngredients = new ArrayList<>();
    private List<LinearLayout> moreIngredientLayout = new ArrayList<>();
    private LinearLayout moreIngredientLinearLayout;
    private ArrayAdapter<String> arrayAdapterIngredient, arrayAdapterCategory;
    private Spinner spinnerIngredient1, spinnerIngredient2, spinnerCocktailCategory;
    private List<Recipe> recipes;
    private int originalRecipesSize;
    private User user;


    public AddNewCocktailPresenter(AddNewCocktailFragmentInterface addNewCocktailFragmentInterface,
                                   Context context, LinearLayout moreIngredientLinearLayout,
                                   User user) {
        this.addNewCocktailFragmentInterface = addNewCocktailFragmentInterface;
        this.context = context;
        this.moreIngredientLinearLayout = moreIngredientLinearLayout;
        this.user = user;
    }

    @Override
    public void checkNewCocktailDetails(String cocktailName, String description, String unit1,
                                        String unit2, Drawable myDrawable) {
        if (!cocktailName.equals("")) {
            if (myDrawable != null) {
                if (!description.equals("")) {
                    if (!unit1.equals("") && !unit2.equals("")) {

                        recipes = new ArrayList<>();
                        UserCocktail userCocktail = new UserCocktail(cocktailName,
                                spinnerCocktailCategory.getSelectedItem().toString(),
                                description, "image", false, user);

                        recipes.add(new Recipe(Float.valueOf(unit1),
                                new Ingredient((int) (spinnerIngredient1.getSelectedItemId() + 1),
                                        spinnerIngredient1.getSelectedItem().toString())));
                        recipes.add(new Recipe(Float.valueOf(unit2),
                                new Ingredient((int) (spinnerIngredient2.getSelectedItemId() + 1),
                                        spinnerIngredient2.getSelectedItem().toString())));

                        if (!moreIngredients.isEmpty()) {
                            for (int i = 0; i < moreIngredients.size(); i++) {
                                recipes.add(new Recipe(Float.valueOf(moreIngredients.get(i).getEditText().getText().toString()),
                                        new Ingredient((int) (moreIngredients.get(i).getSpinner().getSelectedItemId() + 1),
                                                moreIngredients.get(i).getSpinner().getSelectedItem().toString())));
                            }
                        }
                        userCocktail.setRecipes(recipes);

                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        Bitmap anImage = ((BitmapDrawable) myDrawable).getBitmap();
                        Bitmap resized = scaleDown(anImage, 400, true);
                        resized.compress(Bitmap.CompressFormat.JPEG, 80, stream);
                        File f = new File(context.getCacheDir(), "file.jpg");
                        byte[] byteArray = stream.toByteArray();
                        try {
                            f.createNewFile();
                            FileOutputStream fos = new FileOutputStream(f);
                            fos.write(byteArray);
                            fos.flush();
                            fos.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), f);
                        MultipartBody.Part body = MultipartBody.Part.createFormData("file", f.getName(), reqFile);
                        saveCocktail(userCocktail, body);
                    } else {
                        addNewCocktailFragmentInterface.showErrorMessage(R.string.noUnit);
                    }
                } else {
                    addNewCocktailFragmentInterface.showErrorMessage(R.string.noDescription);
                }
            } else {
                addNewCocktailFragmentInterface.showErrorMessage(R.string.noImage);
            }
        } else {
            addNewCocktailFragmentInterface.showErrorMessage(R.string.noCocktailName);
        }
    }

    @Override
    public Bitmap scaleDown(Bitmap realImage, float maxImageSize, boolean filter) {
        float ratio = Math.min(
                (float) maxImageSize / realImage.getWidth(),
                (float) maxImageSize / realImage.getHeight());
        int width = Math.round((float) ratio * realImage.getWidth());
        int height = Math.round((float) ratio * realImage.getHeight());

        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width, height, filter);
        return newBitmap;
    }

    @Override
    public void saveCocktail(final UserCocktail userCocktail, MultipartBody.Part body) {
        addNewCocktailFragmentInterface.showProgressDialog();
        if (cocktailId != null) {
            userCocktail.setId(cocktailId);
            Call<String> listCall = userCocktailService.editUserCocktail(userCocktail.getId(), userCocktail, body);
            listCall.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    updateLocalUserCocktail(userCocktail);
                    addNewCocktailFragmentInterface.showMessage(R.string.succesful);
                    addNewCocktailFragmentInterface.hideProgressDialog();
                    addNewCocktailFragmentInterface.openUserCocktailFragment();
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    t.printStackTrace();
                    addNewCocktailFragmentInterface.hideProgressDialog();
                }
            });
        } else {
            Call<String> listCall = userCocktailService.upload(userCocktail, body);
            listCall.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    saveCocktailToLocalDatabase(userCocktail);
                    addNewCocktailFragmentInterface.showMessage(R.string.succesful);
                    addNewCocktailFragmentInterface.hideProgressDialog();
                    addNewCocktailFragmentInterface.openUserCocktailFragment();
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    t.printStackTrace();
                    addNewCocktailFragmentInterface.hideProgressDialog();
                }
            });
        }
    }

    @Override
    public void saveCocktailToLocalDatabase(final UserCocktail userCocktail) {
        class SaveUserCocktail extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... voids) {
                long newCocktailId = DatabaseClient.getInstance(context).getMixerAppDatabase()
                        .userCocktailDao().save(userCocktail);

                UserCocktail newUserCocktail1 = DatabaseClient.getInstance(context).getMixerAppDatabase()
                        .userCocktailDao().findById((int) newCocktailId);

                for (Recipe recipe : userCocktail.getRecipes()) {
                    recipe.setCocktail(newUserCocktail1);
                    DatabaseClient.getInstance(context).getMixerAppDatabase().recipeDao().save(recipe);
                }
                return null;
            }
        }
        SaveUserCocktail saveUserCocktail = new SaveUserCocktail();
        saveUserCocktail.execute();
    }

    @Override
    public void updateLocalUserCocktail(final UserCocktail userCocktail) {
        class UpdateUserCocktail extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... voids) {
                DatabaseClient.getInstance(context).getMixerAppDatabase()
                        .userCocktailDao().update(userCocktail);

                for(int i = 0; i < originalRecipesSize; i++){
                    DatabaseClient.getInstance(context).getMixerAppDatabase().
                            recipeDao().deleteRecipeByCocktailId(cocktailId);
                }

                for (Recipe recipe : userCocktail.getRecipes()) {
                    recipe.setCocktail(userCocktail);
                    DatabaseClient.getInstance(context).getMixerAppDatabase().
                            recipeDao().save(recipe);
                }
                return null;
            }
        }
        UpdateUserCocktail updateUserCocktail = new UpdateUserCocktail();
        updateUserCocktail.execute();
    }

    @Override
    public void lessIngredient() {
        int last = moreIngredients.size() - 1;
        if (last >= 0) {
            moreIngredients.remove(moreIngredients.get(last));
            moreIngredientLinearLayout.removeView(moreIngredientLayout.get(last));
            moreIngredientLayout.remove(moreIngredientLayout.get(last));
        }
        if (moreIngredients.isEmpty()) {
            moreIngredientLinearLayout.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void moreIngredient(List<Recipe> recipes) {
        LinearLayout li = new LinearLayout(context);
        float pixels = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 190,
                context.getResources().getDisplayMetrics());

        float pixel = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 60,
                context.getResources().getDisplayMetrics());

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams((int) pixels,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(10, 10, 20, 10);

        Spinner newSpinner = new Spinner(context);
        TextView txtUnit = new TextView(context);
        EditText edit = new EditText(context);

        newSpinner.setAdapter(arrayAdapterIngredient);
        newSpinner.setLayoutParams(params);

        txtUnit.setText(R.string.unit);
        txtUnit.setTextSize(20);
        txtUnit.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        txtUnit.setTextColor(Color.BLACK);
        edit.setWidth((int) pixel);
        edit.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        edit.setTextSize(20);
        edit.setTextColor(Color.BLACK);
        edit.setInputType(InputType.TYPE_CLASS_NUMBER);

        if (recipes != null) {
            originalRecipesSize = recipes.size() +2 ;
            for (int i = 2; i < recipes.size(); i++) {
                li = new LinearLayout(context);
                Spinner newSpinner1 = new Spinner(context);
                TextView txtUnit1 = new TextView(context);
                EditText edit1 = new EditText(context);

                newSpinner1.setAdapter(arrayAdapterIngredient);
                newSpinner1.setLayoutParams(params);
                newSpinner1.setSelection(recipes.get(i).getIngredient().getId() - 1);

                txtUnit1.setText(R.string.unit);
                txtUnit1.setTextSize(20);
                txtUnit1.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
                txtUnit1.setTextColor(Color.BLACK);

                edit1.setWidth((int) pixel);
                edit1.setTextSize(20);
                edit1.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
                edit1.setInputType(InputType.TYPE_CLASS_NUMBER);
                edit1.setTextColor(Color.BLACK);
                edit1.setText(String.valueOf(recipes.get(i).getQuantity()));

                li.addView(newSpinner1);
                li.addView(txtUnit1);
                li.addView(edit1);

                moreIngredientLayout.add(li);
                moreIngredients.add(new MoreIngredient(newSpinner1, txtUnit1, edit1));
                moreIngredientLinearLayout.setVisibility(View.VISIBLE);
                moreIngredientLinearLayout.addView(li);
            }
        } else {
            li.addView(newSpinner);
            li.addView(txtUnit);
            li.addView(edit);

            moreIngredientLayout.add(li);
            moreIngredientLinearLayout.setVisibility(View.VISIBLE);
            moreIngredients.add(new MoreIngredient(newSpinner, txtUnit, edit));
            moreIngredientLinearLayout.addView(li);
        }
    }

    @Override
    public void setCocktailId(int cocktailId) {
        this.cocktailId = cocktailId;
    }

    @Override
    public void setSpinner(Spinner spinnerIngredient1, Spinner spinnerIngredient2, Spinner spinnerCocktailCategory,
                           List<String> ingredients,List<String> categoryList) {
        this.spinnerIngredient1 = spinnerIngredient1;
        this.spinnerIngredient2 = spinnerIngredient2;
        this.spinnerCocktailCategory = spinnerCocktailCategory;
        arrayAdapterIngredient = new ArrayAdapter<String>(context, R.layout.support_simple_spinner_dropdown_item, ingredients);
        arrayAdapterIngredient.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        arrayAdapterCategory = new ArrayAdapter<String>(context, R.layout.support_simple_spinner_dropdown_item, categoryList);
        arrayAdapterCategory.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.spinnerCocktailCategory.setAdapter(arrayAdapterCategory);
        this.spinnerIngredient1.setAdapter(arrayAdapterIngredient);
        this.spinnerIngredient2.setAdapter(arrayAdapterIngredient);
    }
}