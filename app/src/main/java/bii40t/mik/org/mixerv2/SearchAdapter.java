package bii40t.mik.org.mixerv2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import bii40t.mik.org.mixerv2.model.cocktail.Cocktail;
import bii40t.mik.org.mixerv2.cocktailDetailFragment.CocktailDetailFragment;
import bii40t.mik.org.mixerv2.mainactivity.MainActivity;
import bii40t.mik.org.mixerv2.model.recipe.Recipe;

public class SearchAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private List<Cocktail> cocktails;
    private ArrayList<Cocktail> arraylist;
    private MainActivity mainActivity;

    public SearchAdapter(MainActivity mainActivity, List<Cocktail> cocktails) {
        this.mainActivity = mainActivity;
        this.cocktails = cocktails;
        inflater = LayoutInflater.from(mainActivity.getBaseContext());
        this.arraylist = new ArrayList<>();
        this.arraylist.addAll(cocktails);
    }

    public class ViewHolder {
        TextView textView;
        ImageView image;
    }

    @Override
    public int getCount() {
        return cocktails.size();
    }

    @Override
    public Cocktail getItem(int i) {
        return cocktails.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        final ViewHolder holder;
        CardView cardViw;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.cardview_favourite_cocktail, null);

            holder.textView = (TextView) view.findViewById(R.id.txtCoctailName);
            holder.image = (ImageView) view.findViewById(R.id.imgViewCoctail);
            cardViw = view.findViewById(R.id.cardView);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
            cardViw = view.findViewById(R.id.cardView);
        }

        holder.textView.setText(cocktails.get(i).getCocktailName());
        PicassoTrustAll.getInstance(mainActivity.getBaseContext())
                .load(MainActivity.url + "image/" + cocktails.get(i).getId())
                .into(holder.image);

        cardViw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Gson gson = new Gson();
                Bundle bundle = new Bundle();
                bundle.putString("Cocktail", gson.toJson(cocktails.get(i)));
                CocktailDetailFragment cocktailDetail = new CocktailDetailFragment();
                cocktailDetail.setArguments(bundle);
                AppCompatActivity activity = (AppCompatActivity) view.getContext();
                activity.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, cocktailDetail).addToBackStack(null).commit();
                mainActivity.hideListView();
            }
        });
        return view;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        cocktails.clear();
        if (charText.length() == 0) {
            cocktails.addAll(arraylist);
        } else {
            for (Cocktail cocktail : arraylist) {
                if (cocktail.getCocktailName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    cocktails.add(cocktail);
                }
                for (Recipe recipe : cocktail.getRecipes()) {
                    if (recipe.getIngredient().getName().toLowerCase().contains(charText) && recipe.getIngredient() != null) {
                        if (!cocktails.contains(cocktail)) {
                            cocktails.add(cocktail);
                        }
                    }
                }
            }
        }
        notifyDataSetChanged();
    }
}