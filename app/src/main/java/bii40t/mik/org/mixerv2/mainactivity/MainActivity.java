package bii40t.mik.org.mixerv2.mainactivity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import bii40t.mik.org.mixerv2.HttpsUtil;
import bii40t.mik.org.mixerv2.R;
import bii40t.mik.org.mixerv2.SearchAdapter;
import bii40t.mik.org.mixerv2.model.cocktail.Cocktail;
import bii40t.mik.org.mixerv2.fragment.HomeFragment;
import bii40t.mik.org.mixerv2.userCocktailFragment.UserCocktailFragment;
import bii40t.mik.org.mixerv2.fragment.BaseFragment;
import bii40t.mik.org.mixerv2.favouriteCocktailFragment.FavouriteCocktailFragment;
import bii40t.mik.org.mixerv2.fragment.SettingsFragment;
import bii40t.mik.org.mixerv2.login.LogInActivity;
import bii40t.mik.org.mixerv2.signup.SignUpActivity;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener, MainActivityViewInterface {

    public static final String url = "https://35.240.123.252:8443/";//35.240.123.252 - szerver ,80.252.50.208 - zoli otthoni
    private SharedPreferences sharedPreferencesLogIn;
    private SharedPreferences sharedPreferencesLanguage;
    private String language;
    private SearchView searchView;
    private ListView listView;
    private SearchAdapter searchAdapter;
    private ArrayList<Cocktail> cocktailArra = new ArrayList<>();
    private ProgressDialog progressDialog;
    private Gson gson = new Gson();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferencesLanguage = getSharedPreferences("language", MODE_PRIVATE);
        language = sharedPreferencesLanguage.getString("language", null);
        if (language == null) {
            language = "en";
        }
        Locale locale = new Locale(language);
        Configuration config = getBaseContext().getResources().getConfiguration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext()
                .getResources().getDisplayMetrics());
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        sharedPreferencesLogIn = getSharedPreferences("login", MODE_PRIVATE);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(this);
        listView = findViewById(R.id.searcCocktail);
        listView.setVisibility(View.INVISIBLE);

        HttpsUtil httpsUtil = new HttpsUtil(getApplication());
        MainActivityPresenter mainActivityPresenter
                = new MainActivityPresenter(this, getBaseContext());

        if (httpsUtil.isOnline()) {
            mainActivityPresenter.getCocktailsFromServer();
        } else {
            mainActivityPresenter.getCocktailsFromLocalDatabase();
        }
        hideKeyboard();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void hideKeyboard() {
        listView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(searchView.getWindowToken(), 0);
                return false;
            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;
        if (sharedPreferencesLogIn.getBoolean("savelogin", false)) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    hideListView();
                    fragment = new HomeFragment();
                    break;

                case R.id.navigation_favourite:
                    hideListView();
                    fragment = new FavouriteCocktailFragment();
                    break;

                case R.id.navigation_my_cocktail:
                    hideListView();
                    fragment = new UserCocktailFragment();
                    break;
            }
        } else {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    fragment = new HomeFragment();
                    break;

                case R.id.navigation_favourite:
                    Toast.makeText(this.getApplicationContext(), R.string.mustLogin, Toast.LENGTH_SHORT).show();
                    break;

                case R.id.navigation_my_cocktail:
                    Toast.makeText(this.getApplicationContext(), R.string.mustLogin, Toast.LENGTH_SHORT).show();
                    break;
            }
        }
        return loadFragment(fragment);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        MenuItem searchItem;

        if (sharedPreferencesLogIn.getBoolean("savelogin", false)) {
            menuInflater.inflate(R.menu.log_out, menu);
            searchItem = menu.findItem(R.id.serach2);
            searchView = (SearchView) searchItem.getActionView();
        } else {
            menuInflater.inflate(R.menu.sing_in_up, menu);
            searchItem = menu.findItem(R.id.serach3);
            searchView = (SearchView) searchItem.getActionView();
        }

        searchView.setQueryHint(getResources().getString(R.string.search));
        searchView.setIconifiedByDefault(true);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                listView.setVisibility(View.VISIBLE);
                searchAdapter.filter(s);
                listView.setAdapter(searchAdapter);
                return false;
            }
        });

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                hideListView();
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    fragment).commit();
            return true;
        }
        return false;
    }

    @Override
    public void showProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
    }

    @Override
    public void hideProgressDialog() {
        progressDialog.dismiss();
    }

    @Override
    public void settings(MenuItem item) {
        loadFragment(new SettingsFragment());
    }

    @Override
    public void logOut(MenuItem item) {
        SharedPreferences sharedPreferences = getSharedPreferences("login", MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
        finish();
        startActivity(getIntent());
    }

    @Override
    public void logIn(MenuItem item) {
        startActivity(new Intent(this, LogInActivity.class));
    }

    @Override
    public void signUp(MenuItem item) {
        startActivity(new Intent(this, SignUpActivity.class));
    }

    @Override
    public void hideListView() {
        searchView.setQuery("", false);
        listView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onBackPressed() {
        List<Fragment> fragmentList = getSupportFragmentManager().getFragments();
        boolean handled = false;
        for (Fragment f : fragmentList) {
            if (f instanceof BaseFragment) {
                handled = ((BaseFragment) f).onBackPressed();
                if (handled) {
                    break;
                }
            }
        }
        if (!handled) {
            super.onBackPressed();
        }
        hideListView();
    }

    @Override
    public void searchList(List<Cocktail> cocktails1) {
        for (int i = 0; i < cocktails1.size(); i++) {
            cocktailArra.add(cocktails1.get(i));
        }
        searchAdapter = new SearchAdapter(this, cocktailArra);
    }

    @Override
    public void addCocktailToPreferenciaManager(List<Cocktail> cocktails) {
        String cocktailListString = gson.toJson(cocktails);
        PreferenceManager.getDefaultSharedPreferences(getApplication()).edit().putString("Cocktails", cocktailListString).commit();
    }
}