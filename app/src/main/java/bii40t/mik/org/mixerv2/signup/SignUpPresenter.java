package bii40t.mik.org.mixerv2.signup;

import bii40t.mik.org.mixerv2.ApiClient;
import bii40t.mik.org.mixerv2.AESCrypt;
import bii40t.mik.org.mixerv2.R;
import bii40t.mik.org.mixerv2.model.user.User;
import bii40t.mik.org.mixerv2.model.user.UserService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpPresenter implements SignUpPresenterInterface {

    private SignUpViewInterface signUpViewInterface;
    private UserService userService = ApiClient.getClient().create(UserService.class);
    private AESCrypt AESCrypt = new AESCrypt();

    public SignUpPresenter(SignUpViewInterface signUpViewInterface) {
        this.signUpViewInterface = signUpViewInterface;
    }

    @Override
    public void checkSignUpDetails(String userName, String password, String passwordAgain) {
        if(userName.equals("")){
            signUpViewInterface.showErrorMessage(R.string.emptyUserName);
        } else if(password.equals("")){
            signUpViewInterface.showErrorMessage(R.string.emptyPassWd);
        } else if(password.toCharArray().length < 8){
            signUpViewInterface.showErrorMessage(R.string.shortPasswd);
        }else if(!password.equals(passwordAgain)){
            signUpViewInterface.showErrorMessage(R.string.diffPassWd);
        }else{
            User user = new User(userName, AESCrypt.encrypt(password).toCharArray());
            signUp(user);
        }
    }

    @Override
    public void signUp(final User user) {
        Call<String> call = userService.createUser(user);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                String data = response.body();
                if (data != null && response != null) {
                    if (data.equals("ok")) {
                        signUpViewInterface.startActivity(user);
                    } else {
                        signUpViewInterface.showErrorMessage(R.string.alreadyExist);
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}
