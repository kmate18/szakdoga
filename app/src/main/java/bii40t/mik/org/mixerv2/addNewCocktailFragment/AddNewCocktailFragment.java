package bii40t.mik.org.mixerv2.addNewCocktailFragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import bii40t.mik.org.mixerv2.ApiClient;
import bii40t.mik.org.mixerv2.HttpsUtil;
import bii40t.mik.org.mixerv2.fragment.BaseFragment;
import bii40t.mik.org.mixerv2.mainactivity.MainActivity;
import bii40t.mik.org.mixerv2.PicassoTrustAll;
import bii40t.mik.org.mixerv2.R;
import bii40t.mik.org.mixerv2.model.cocktail.Cocktail;
import bii40t.mik.org.mixerv2.model.ingredient.Ingredient;
import bii40t.mik.org.mixerv2.model.ingredient.IngredientService;
import bii40t.mik.org.mixerv2.model.recipe.Recipe;
import bii40t.mik.org.mixerv2.model.user.User;
import bii40t.mik.org.mixerv2.model.userCocktail.UserCocktail;
import bii40t.mik.org.mixerv2.userCocktailFragment.UserCocktailFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddNewCocktailFragment extends BaseFragment implements AddNewCocktailFragmentInterface {

    private EditText editTextUnit1, editTextUnit2, editTextCocktailName, editTextDescription;
    private Spinner spinnerCocktailCategory, spinnerIngredient1, spinnerIngredient2;
    private Button btnMoreIngredient, btnSave, btnTakePicture, btnLessIngredient;
    private ImageView imageView;
    private List<String> categoryList = new ArrayList<>();
    private List<String> ingredientList;
    private SharedPreferences sharedPreferences;
    private LinearLayout moreIngredientLinearLayout;
    private Bundle bundle;
    private Gson gson = new Gson();
    private Integer REQUEST_CAMERA = 1, SELECT_FILE = 0;
    private static final int MY_CAMERA_REQUEST_CODE = 100;
    private ProgressDialog progressDialog ;
    private AddNewCocktailPresenter addNewCocktailPresenter;

    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_new_cocktail, container, false);
        editTextCocktailName = view.findViewById(R.id.editTextNewCocktailName);
        editTextUnit1 = view.findViewById(R.id.editTextUnit);
        editTextUnit2 = view.findViewById(R.id.editTextUnit1);
        editTextDescription = view.findViewById(R.id.editTextDescription);
        spinnerCocktailCategory = view.findViewById(R.id.dropDownListCategory);
        spinnerIngredient1 = view.findViewById(R.id.dropDownListIngredient);
        spinnerIngredient2 = view.findViewById(R.id.dropDownListIngredient1);
        btnMoreIngredient = view.findViewById(R.id.btnMoreIngredient);
        btnSave = view.findViewById(R.id.btnSaveNewCocktail);
        btnTakePicture = view.findViewById(R.id.btnImage);
        imageView = view.findViewById(R.id.imageView);
        sharedPreferences = this.getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        moreIngredientLinearLayout = view.findViewById(R.id.moreIngredient1);
        moreIngredientLinearLayout.setVisibility(View.INVISIBLE);
        btnLessIngredient = view.findViewById(R.id.btnLessIngredient);
        NestedScrollView nestedScrollView = view.findViewById(R.id.addNewCocktailScrollV);
        imageView.setImageResource(R.drawable.no_image);
        progressDialog = new ProgressDialog(getActivity());
        String json = sharedPreferences.getString("User", "");
        User user = gson.fromJson(json, User.class);

        addNewCocktailPresenter = new AddNewCocktailPresenter(this,
                getContext(),moreIngredientLinearLayout,user);

        String cocktailsString = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("Cocktails", "");
        List<Cocktail> cocktails = gson.fromJson(cocktailsString, new TypeToken<List<Cocktail>>(){}.getType());

        nestedScrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view2, MotionEvent motionEvent) {
                InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                View focusedView = getActivity().getCurrentFocus();
                if (focusedView != null) {
                    inputManager.hideSoftInputFromWindow(focusedView.getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                }
                return false;
            }
        });

        for (int i = 0; i < cocktails.size(); i++) {
            if (!categoryList.contains(cocktails.get(i).getCategory())) {
                categoryList.add(cocktails.get(i).getCategory());
            }
        }
        getIngredient();
        bundle = this.getArguments();
        if (bundle != null) {
            edit();
        }
        final HttpsUtil httpsUtil = new HttpsUtil(getContext());

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(httpsUtil.isOnline()){
                    addNewCocktailPresenter.checkNewCocktailDetails(editTextCocktailName.getText().toString(),
                            editTextDescription.getText().toString(), editTextUnit1.getText().toString(),
                            editTextUnit2.getText().toString(),
                            imageView.getDrawable());
                }
            }
        });
        btnMoreIngredient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addNewCocktailPresenter.moreIngredient(null);
            }
        });
        btnTakePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePhoto();
            }
        });
        btnLessIngredient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addNewCocktailPresenter.lessIngredient();
            }
        });
        return view;
    }

    @Override
    public void getIngredient() {
        IngredientService favouriteCocktailService = ApiClient.getClient().create(IngredientService.class);
        Call<List<Ingredient>> listCall = favouriteCocktailService.getIngredients();
        listCall.enqueue(new Callback<List<Ingredient>>() {
            @Override
            public void onResponse(Call<List<Ingredient>> call, Response<List<Ingredient>> response) {
                List<Ingredient> ingredients = response.body();
                ingredientList = new ArrayList<>();
                for (Ingredient ingredient: ingredients) {
                    ingredientList.add(ingredient.getName() +" (" + ingredient.getUnit()+")");
                }
                addNewCocktailPresenter.setSpinner(spinnerIngredient1, spinnerIngredient2,
                        spinnerCocktailCategory, ingredientList, categoryList);
            }

            @Override
            public void onFailure(Call<List<Ingredient>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public void takePhoto() {
        final CharSequence[] items = {getResources().getString(R.string.camera)
                , getResources().getString(R.string.album)
                , getResources().getString(R.string.cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getResources().getString(R.string.addImage));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (items[i].equals(getResources().getString(R.string.camera))) {
                    if (getContext().checkCallingOrSelfPermission(Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA},
                                MY_CAMERA_REQUEST_CODE);
                    } else {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, REQUEST_CAMERA);
                    }

                } else if (items[i].equals(getResources().getString(R.string.album))) {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(intent, SELECT_FILE);

                } else if (items[i].equals(getResources().getString(R.string.cancel))) {
                    dialogInterface.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                Uri selectedImageUri = data.getData();
                imageView.setImageURI(selectedImageUri);
            } else if (requestCode == REQUEST_CAMERA) {
                Bundle bundle = data.getExtras();
                final Bitmap bmp = (Bitmap) bundle.get("data");
                imageView.setImageBitmap(bmp);
            }
        }
    }

    @Override
    public void edit() {
        String userCocktailJson = bundle.getString("EditCocktail");
        UserCocktail userCocktail = gson.fromJson(userCocktailJson, UserCocktail.class);
        editTextCocktailName.setText(userCocktail.getCocktailName());
        editTextDescription.setText(userCocktail.getDescription());
        List<Recipe> lstRecipe = userCocktail.getRecipes();

        editTextUnit1.setText(String.valueOf(lstRecipe.get(0).getQuantity()));
        editTextUnit2.setText(String.valueOf(lstRecipe.get(1).getQuantity()));

        spinnerIngredient1.setSelection(lstRecipe.get(0).getIngredient().getId() - 1);
        spinnerIngredient2.setSelection(lstRecipe.get(1).getIngredient().getId() - 1);
        spinnerCocktailCategory.setSelection(categoryList.indexOf(userCocktail.getCategory()));

        PicassoTrustAll.getInstance(getContext())
                .load(MainActivity.url + "image/" + userCocktail.getId())
                .into(imageView);
        addNewCocktailPresenter.setCocktailId(userCocktail.getId());
        addNewCocktailPresenter.moreIngredient(lstRecipe);
    }

    @Override
    public boolean onBackPressed() {
        this.getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, new UserCocktailFragment()).commit();
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_CAMERA_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, REQUEST_CAMERA);
                Toast.makeText(getActivity(), getResources().getString(R.string.cameraPermissionOk), Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getActivity(), getResources().getString(R.string.cameraPermissionCancel), Toast.LENGTH_LONG).show();
            }
        }
    }
    @Override
    public void showErrorMessage(int id) {
        Toast.makeText(getContext(),id,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showMessage(int id) {
        Toast.makeText(getContext(),id,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgressDialog() {
        progressDialog.setMessage(getResources().getString(R.string.upload));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
    }

    @Override
    public void hideProgressDialog() {
        progressDialog.hide();
    }

    @Override
    public void openUserCocktailFragment() {
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, new UserCocktailFragment()).commit();
    }
}