package bii40t.mik.org.mixerv2.mainactivity;

import java.util.List;

import bii40t.mik.org.mixerv2.model.cocktail.Cocktail;

public interface MainActivityPresenterInterface {
    void getCocktailsFromServer();

    void saveCocktails(List<Cocktail> cocktails);

    void getCocktailsFromLocalDatabase();
}