package bii40t.mik.org.mixerv2.model.favorite;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface FavouriteCocktailDao {

    @Query("select * from favourites where favourites.username =:userName")
    public List<FavouriteCocktail> findAllFavouriteCocktailByUserName(String userName);

    @Insert
    public void save(FavouriteCocktail favouriteCocktail);

    @Query("delete from favourites where  identifier = :identifier")
    public void deleteFavouriteCocktailById(String identifier);
}