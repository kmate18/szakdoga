package bii40t.mik.org.mixerv2.model.favorite;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import java.util.Objects;

import bii40t.mik.org.mixerv2.model.user.User;


@Entity(tableName = FavouriteCocktail.TBL_FAVOURITE)
public class FavouriteCocktail {

    public static final String TBL_FAVOURITE = "favourites";
    public static final String COL_COCKTAIL_ID = "cocktail_id";
    public static final String COL_IDENTIFIER = "identifier";

    @PrimaryKey(autoGenerate = true)
    private Integer favourite_id;

    @ColumnInfo(name = COL_COCKTAIL_ID)
    private int cocktailId;

    @ColumnInfo(name = COL_IDENTIFIER)
    private String identifier;

    @Embedded
    private User user;

    public FavouriteCocktail(int cocktailId, String identifier, User user) {
        this.cocktailId = cocktailId;
        this.identifier = identifier;
        this.user = user;
    }

    public Integer getFavourite_id() {
        return favourite_id;
    }

    public void setFavourite_id(Integer favourite_id) {
        this.favourite_id = favourite_id;
    }

    public int getCocktailId() {
        return cocktailId;
    }

    public void setCocktailId(int cocktailId) {
        this.cocktailId = cocktailId;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FavouriteCocktail that = (FavouriteCocktail) o;
        return cocktailId == that.cocktailId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(cocktailId);
    }
}
