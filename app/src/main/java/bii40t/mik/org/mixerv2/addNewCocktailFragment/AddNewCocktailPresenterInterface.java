package bii40t.mik.org.mixerv2.addNewCocktailFragment;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.widget.Spinner;

import java.util.List;

import bii40t.mik.org.mixerv2.model.recipe.Recipe;
import bii40t.mik.org.mixerv2.model.userCocktail.UserCocktail;
import okhttp3.MultipartBody;

public interface AddNewCocktailPresenterInterface {

    void checkNewCocktailDetails(String cocktailName,String description,String unit1,String unit2, Drawable myDrawable);

    Bitmap scaleDown(Bitmap realImage, float maxImageSize, boolean filter);

    void saveCocktail(final UserCocktail userCocktail, MultipartBody.Part body);

    void saveCocktailToLocalDatabase(final UserCocktail userCocktail);

    void updateLocalUserCocktail(final UserCocktail userCocktail);

    void lessIngredient();

    void moreIngredient(List<Recipe> recipes);

    void setCocktailId(int cocktailId);

    void setSpinner(Spinner spinnerIngredient1, Spinner spinnerIngredient2, Spinner categorySpinner, List<String> ingredients, List<String> categoryList);
}
