package bii40t.mik.org.mixerv2.model.userCocktail;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface UserCocktailService {

    @GET("cocktail")
    Call<List<UserCocktail>> getCocktails();

    @GET("usercocktail/user/{userName}")
    Call<List<UserCocktail>> getUserCocktails(@Path("userName") String userName);

    @Multipart
    @POST("cocktail/save")
    Call<String> upload(@Part("userCocktail") UserCocktail userCocktail,
                        @Part MultipartBody.Part image);

    @Multipart
    @PUT("usercocktail/edit/{id}")
    Call<String> editUserCocktail(@Path("id") int id,
                                  @Part("userCocktail") UserCocktail userCocktail,
                                  @Part MultipartBody.Part image);

    @PUT("usercocktail/share/{id}")
    Call<String> shareCocktail(@Path("id") int id);

    @DELETE("usercocktail/delete/{id}")
    Call<Void> deleteUserCocktail(@Path("id") int id);
}
