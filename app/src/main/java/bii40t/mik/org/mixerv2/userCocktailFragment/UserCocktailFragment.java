package bii40t.mik.org.mixerv2.userCocktailFragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.List;

import bii40t.mik.org.mixerv2.HttpsUtil;
import bii40t.mik.org.mixerv2.addNewCocktailFragment.AddNewCocktailFragment;
import bii40t.mik.org.mixerv2.fragment.BaseFragment;
import bii40t.mik.org.mixerv2.fragment.HomeFragment;
import bii40t.mik.org.mixerv2.R;
import bii40t.mik.org.mixerv2.model.user.User;
import bii40t.mik.org.mixerv2.model.userCocktail.UserCocktail;
import bii40t.mik.org.mixerv2.viewAdapter.UserCocktailRecyclerViewAdapter;

public class UserCocktailFragment extends BaseFragment implements UserCocktailFragmentInterface {

    private RecyclerView myView;
    private TextView txtMyCocktail;
    private FloatingActionButton floatingActionButtonToAddNewCocktail;
    private SharedPreferences sharedPreferences;
    private ProgressDialog progressDialog;
    private User user;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_cocktail, container, false);
        sharedPreferences = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);

        Gson gson = new Gson();
        String json = sharedPreferences.getString("User", "");
        user = gson.fromJson(json, User.class);

        txtMyCocktail = view.findViewById(R.id.textViewFavouriteCoktail);
        floatingActionButtonToAddNewCocktail = view.findViewById(R.id.fabAddNewCocktail);
        txtMyCocktail.setText(R.string.myCocktails);

        floatingActionButtonToAddNewCocktail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new AddNewCocktailFragment()).commit();
            }
        });

        myView = (RecyclerView) view.findViewById(R.id.coctailRecyclerView);
        UserCocktailPresenter userCocktailPresenter = new UserCocktailPresenter(this, user, getContext());
        HttpsUtil httpsUtil = new HttpsUtil(getContext());
        if (httpsUtil.isOnline()) {
            userCocktailPresenter.getUserCocktailFromServer();
        } else {
            userCocktailPresenter.getUserCocktailFromLocalDatabase();
        }
        return view;
    }

    @Override
    public void showProgressDialog() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.show();
    }

    @Override
    public void hideProgressDialog() {
        progressDialog.dismiss();
    }

    @Override
    public void setMyCocktailRecycleViewAdapter(List<UserCocktail> userCocktails) {
        UserCocktailRecyclerViewAdapter myAdapter
                = new UserCocktailRecyclerViewAdapter(this.getContext(), userCocktails);
        myView.setLayoutManager(new GridLayoutManager(this.getContext(), 1));
        myView.setAdapter(myAdapter);
    }

    @Override
    public boolean onBackPressed() {
        this.getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).commit();
        return true;
    }
}