package bii40t.mik.org.mixerv2.signup;

import bii40t.mik.org.mixerv2.model.user.User;

public interface SignUpViewInterface {

    void showErrorMessage(int id);

    void startActivity(User user);
}