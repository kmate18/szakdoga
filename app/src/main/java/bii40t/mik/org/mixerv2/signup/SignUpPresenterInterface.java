package bii40t.mik.org.mixerv2.signup;

import bii40t.mik.org.mixerv2.model.user.User;

public interface SignUpPresenterInterface {

    void checkSignUpDetails(String userName, String password, String passwordAgain);

    void signUp(User user);
}